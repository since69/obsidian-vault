# Design Phase

- Period - 2025.2.10 ~ 2025.3.30

### Artifacts

| Name            | Start   | End | Owner |
| --------------- | ------- | --- | ----- |
| 기능명세서           | 25.2.10 |     |       |
| Usecase Diagram |         |     |       |
| Class Digram    |         |     |       |
| API Design      |         |     |       |

