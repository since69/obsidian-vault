---
title: Cloud Native Transformation Emergency
Status: In Progress
tags:
  - project
  - fire
preiod: 2025.01.01 ~ 25.09.23
Deadline: 2025-09-23
---
# Cloud Native Transformation Emergency

### Overview

- 사업명 - 24년도 클라우드 네이티브 전환 사업(행안부 긴급구조신고공동관리센터)
- 사업기간 - 계약일로부터 270일(9개월), 25년 9월 23일까지
- 사업내용 - 긴급신고 공동활용 및 대응 시스템, 긴급신고 바로앱
- 사업관리
	- 지능정보사회진흥원 클라우드 전환팀 팁장 조현웅 (053-230-1641)
	- 지능정보사회진흥원 클라우드 전환팀 선임 박혜지 (053-230-1656)

### Events

- 2/10 - 출근 시작
- 2/13 - 대구 공동관리센터 출장

### Environments

- wifi - five0401

### References

#####  Legacy Artifacts


### Stage

| stage                | link                                            | start    | end     |
| -------------------- | ----------------------------------------------- | -------- | ------- |
| Analysis             | [[project/CNTE/stage/analysis\|Analysis Phase]] | 24.12.30 | 25.2.7  |
| Design               | [[project/CNTE/stage/design\|Design]]           | 25.2.10  | 25.3.30 |
| Implementation       |                                                 |          |         |
| Unit Test            |                                                 |          |         |
| Integration Test     |                                                 |          |         |
| User Acceptance Test |                                                 |          |         |
