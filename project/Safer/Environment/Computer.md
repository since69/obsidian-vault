
### `:FasServer:` Server

| Name                            |        IPv4 | OS              | Id             | Password  | Description |
| ------------------------------- | ----------: | --------------- | -------------- | --------- | ----------- |
| AVL                             |  10.10.3.22 | Win Server 2019 | fscd-avl       | Fscd2022! |             |
| Broadcasting #1<br>(10.10.3.20) |  10.10.3.18 | Win server 2019 | fscd-broadast1 | Fscd2022! |             |
| Broadcasting #2<br>(10.10.3.20) |  10.10.3.19 | Win server 2019 | fscd-broadast1 | Fscd2022! |             |
| CTI #1 <br>  (10.10.3.30)       |  10.10.3.28 | Win server 2019 | fscd-cti       | Fscd2022! |             |
| CTI #2 <br> (10.10.3.30)        |  10.10.3.29 | Win server 2019 | fscd-cti       | Fscd2022! |             |
| GIS #1                          |  10.10.3.15 | Rocky linux     | root           | Fscd2022! |             |
| GIS #2                          |  10.10.3.16 | Rocky linux     | root           | Fscd2022! |             |
| DB #1                           |  10.10.3.10 | Rocky linux     | root           | Fscd2022! |             |
| DB #2                           |  10.10.3.11 | Rocky linux     | root           | Fscd2022! |             |
| Interface (Link)                |  10.10.2.16 | Windows server  | fscd-link      | Fscd2022! |             |
| IVR                             |  10.10.3.21 | Windows server  | fscd-ivr       | Fscd2022! |             |
| SMS                             |  10.10.3.27 | Windows server  | fscd-link      | Fscd2022! |             |
| Recording                       | 10.10.3.140 | Windows server  | Adminstrator   | mset1193  |             |
| WAS #1                          |  10.10.2.13 | Redhat linux    | root           | Fscd200!  |             |
| WAS #2                          |  10.10.2.14 | Redhat linux    | root           | Fscd200!  |             |
| Web #1                          |  10.10.2.11 | Redhat linux    | root           | Fscd200!  |             |
| Web #2                          |  10.10.2.12 | Redhat linux    | root           | Fscd200!  |             |


### `:FasDesktop:` Desktop

| Name   |       IPv4 | OS         | Id  | Password | Description |
| ------ | ---------: | ---------- | --- | -------- | ----------- |
| DSP-1  |  10.10.5.1 | Windows 10 |     |          |             |
| DSP-2  |  10.10.5.2 | Windows 10 |     |          |             |
| DSP-3  |  10.10.5.3 | Windows 10 |     |          |             |
| DSP-4  |  10.10.5.4 | Windows 10 |     |          |             |
| DSP-5  |  10.10.5.5 | Windows 10 |     |          |             |
| DSP-6  |  10.10.5.6 | Windows 10 |     |          |             |
| DSP-7  |  10.10.5.7 | Windows 10 |     |          |             |
| DSP-8  |  10.10.5.8 | Windows 10 |     |          |             |
| DSP-9  |  10.10.5.9 | Windows 10 |     |          |             |
| DSP-10 | 10.10.5.11 | Windows 10 |     |          |             |
| DSP-11 | 10.10.5.11 | Windows 10 |     |          |             |
| DSP-12 | 10.10.5.12 | Windows 10 |     |          |             |
| DSP-13 | 10.10.5.13 | Windows 10 |     |          |             |
| DSP-14 | 10.10.5.14 | Windows 10 |     |          |             |
| DSP-15 | 10.10.5.15 | Windows 10 |     |          |             |
| DSP-16 | 10.10.5.16 | Windows 10 |     |          |             |
| DSP-17 | 10.10.5.17 | Windows 10 |     |          |             |
| DSP-18 | 10.10.5.18 | Windows 10 |     |          |             |
| DSP-19 | 10.10.5.19 | Windows 10 |     |          |             |
| DSP-20 | 10.10.5.20 | Windows 10 |     |          |             |
| DSP-21 | 10.10.5.21 | Windows 10 |     |          |             |

