# Levels

### Level 1
### Level 2

### `ris:CheckboxMultipleBlank`Target Station

| Order | Station  | IPv4 | Due Date | Result |
| ---: |----- | ----: | ----- | :-----: |
| 1 | Siddik Bazar | 10.10.30.10 | 7/23 00:00 | N/A |
| 2 | Polashi | 10.10.31.10 | 7/23 00:00 | N/A |
| 3 | Hajaribag | 10.10.33.10 | 7/24 00:00 | N/A |
| 4 | Mohammadpur | 10.10.34.10 | 7/24 00:00 | N/A |
| 5 | Postogola | 10.10.40.10 | 7/25 00:00 | N/A |
| 6 | Khilgaon | 10.10.41.10 | 7/25 00:00 | N/A |



### `ris:History` Test History 

| Station | Order | Date | Result |
| ----- | ----: | ----- | ----- |
| Siddik Bazar | 1 | 7/23 |`ris:Check` ALI 수신 불가 (IF Server)<br/> `ris:Check` GIS 지도 표출 안됨 (DOR) |
| Siddik Bazar | 1 | 7/24 |`ris:Check` ALI 수신 불가 (IF Server)<br/> `ris:Check` 소방서 지도 표시 안됨 (Dotnet 버전 상이)
| Polashi | 1 | 7/24 ||
| Hajaribag | 1 | 7/24 ||
| Mohammadpur | 1 | 7/24 ||
| Postogola | 1 | 7/25 ||
| Khilgaon | 1 | 7/25 ||

