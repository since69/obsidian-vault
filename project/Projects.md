---
author: Joohyoung Kim
modified at: 2024-01-31T21:07:00
---
# My Projects
- [[Sfd]]
- [[Safer]]
- [[CloudeNative]]


## In Progress

```dataview
TABLE Status, Deadline
FROM #project
WHERE contains(Status, "In Progress")
```

## Delayed

```dataview
TABLE Status, Deadline
FROM #project
WHERE contains(Status, "Delayed")
```