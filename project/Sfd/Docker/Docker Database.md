# MariaDB

### Common

##### Starting dockerd

```bash
sudo systemctl start docker
# add user to docker group
sudo gpasswd -a "${USER}" docker
```

##### Download an Image

```bash
# search docker image
docker search mariadb
# pull mariadb latest or specfic version
docker pull mariadb:latest
docker pull mariadb:11.3.2
# check the installed image
docker images
```

##### Running and Stopping the Container

```bash
# restart
docker restart sfddb-maria

# start
docker start sfddb-maria

# stop or kill
docker stop --time=30 sfd-mariadb
docker kill sfddb-maria

# pausing
docker pause sfddb-maria
docker unpause sfddb-maria

# delete container
docker rm -v sfddb-maria
```

##### Automatic Restart

``` bash
docker update --restart always sfddb-maria
# or, to change the restart policy of all containers:
docker update --restart always $(docker ps -q)
```

##### Troubleshooting a Container

```bash
# loggin
docker logs sfddb-maria
```

##### Accessing the Container

```bash
docker exec -it sfddb-maria bash
# use nomar linux command
ls
pwd
```


### Without Dockerfile

##### Creating a Container

```bash
# create container
docker run --name maria-dv -e MYSQL_ROOT_PASSWORD=Skrghk69 -p 3306:3306 -d docker.io/library/mariadb:latest
# check the created container
docker ps -a
```

##### Run script

```bash
docker run --name sfd -v ./instsfddb.sql:/docker-entrypoint-initdb.d/instsfddb.sql -e MARIADB_ROOT_PASSWORD=you_pw -d mariadb
```

  
### With Dockerfile

