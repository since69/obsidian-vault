# WebApp

### Publish .NET app

Before adding the .NET app to the Docker image, first it must be published. It's best to have the container run the published version of the app. To publish the app, run the following command:

```Powershell
dotnet publish -c Release
```

This command compiles your app to the _publish_ folder. The path to the _publish_ folder from the working folder should be `.\App\bin\Release\net8.0\publish\`.

### Create the Dockerfile

The _Dockerfile_ file is used by the `docker build` command to create a container image. This file is a text file named _Dockerfile_ that doesn't have an extension.

Create a file named _Dockerfile_ in the directory containing the _.csproj_ and open it in a text editor. This tutorial uses the ASP.NET Core runtime image (which contains the .NET runtime image) and corresponds with the .NET console application.

- Default
```docker
FROM mcr.microsoft.com/dotnet/sdk:8.0 AS base
WORKDIR /app
EXPOSE 8080
EXPOSE 8080

FROM mcr.microsoft.com/dotnet/sdk:8.0 AS build
WORKDIR /src
COPY ["Sfd.WebApp/Sfd.WebApp.csproj", "Sfd.WebApp/"]
COPY ["Sfd.Data/Sfd.Data.csproj", "Sfd.Data/"]
RUN dotnet restore "Sfd.WebApp/Sfd.WebApp.csproj"
# copy everything else and build app
COPY . .
WORKDIR /src/Sfd.WebApp
RUN dotnet build "Sfd.WebApp.csproj" -c release -o /app

FROM build AS publish
RUN dotnet publish "Sfd.WebApp.csproj" -c release -o /app

# final stage/image
FROM base AS final
WORKDIR /app
COPY --from=publish /app .
ENTRYPOINT [ "dotnet", "Sfd.WebApp.dll" ]
```
- More Complicated
```docker

```

To build the container, from your terminal, run the following command:

```PowerShell
docker build -t sfd-image -f Dockerfile .
```

Run *docker image* to see a list of images installed:

```PowerShell
docker images
```

### Create a Container

```PowerShell
docker create --name sfd sfd-image
```

To see a list of _all_ containers, use the `docker ps -a` command:

```PowerShell
docker ps -a
```

### Manage the container

The container was created with a specific name `sfd`. This name is used to manage the container. The following example uses the `docker start` command to start the container, and then uses the `docker ps` command to only show containers that are running:

```PowerShell
# start container
docker start sfd
# stop container
docker stop sfd
# show docker process list
docker ps
```

### Connect to a container

After a container is running, you can connect to it to see the output. Use the `docker start` and `docker attach` commands to start the container and peek at the output stream. In this example, the Ctrl+C keystroke is used to detach from the running container. This keystroke ends the process in the container unless otherwise specified, which would stop the container. The `--sig-proxy=false` parameter ensures that Ctrl+C won't stop the process in the container.

After you detach from the container, reattach to verify that it's still running and counting.

```PowerShell
# start container
docker start sfd
# attatch to container
docker attach --sig-proxy=false sfd
# stop the process in the container.
^C
```

### Delete a container

For this article, you don't want containers hanging around that don't do anything. Delete the container you previously created. If the container is running, stop it.

```PowerShell
docker stop sfd
```

The following is lists all containers. It then uses the `docker rm` command to delete the container and then checks a second time for any running containers.

```PowerShell
# container list
docker ps -a
# remove container
docker rm sfd
```


### At a Glance

```PowerShell
# move the project folder
cd F:\work\repos\gitlab\since69\sfd\sfd-src\src\Sfd.WebApp
# build project iamge & confirm
# COUTION: must have a Dockerfile ready
docker build -t muhayou/sfd:latest -f Dockerfile .
docker images
# remove docker image
docker rmi [image_id]
docker rmi -f [image_id] # use before remove container
# create container & confirm
docker create --name sfd sfd-image
docker ps -a
# start container
docker start sfd
# stop container
docker stop sfd
# delete container & confirm
docker stop sfd
docker rm sfd
docker ps -a

# run?
# docker run [image-name] [container-name]
docker run -p 8080:80 muhayou/sfd:latest sfd
docker run -itd --rm -p 8080:8080 muhayou/sfd:latest sfd

# push to repository
docker push muhayou/sfd

# pull from repository
docker pull muhayou/sfd:latest
```