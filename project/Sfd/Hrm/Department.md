# Department

# Overview

Smart Fire Department 시스템에서 사용되는 모든 부서들을 포함

# 조직체계

최상위 본부 아래에 지역 본부 및 일반 부서들이 위치하며 검색과 위계를 위해 부서 유형과 부서의 레벨이 추가됨.

### 부서유형

부서를 쉽게 구분하기 위한 정보이며 검색에서 조건으로 활용 됨.

- HQ - 본부
- Branch - 지사
- Station - 소방서 (소방조직에만 사용)
- Center - 안전센터 (소방조직에만 사용)
- General - 일반 예하 부서

### 부서위계

부서의 위계에 대한 정보이며 숫자가 낮을수록 상위 위계이고 검색에서 조건으로 활용 됨.


### 예시

예시는 대한민국 소방조직이며 소방청을 본부로 각 지역본부 및 소방서, 안전센터 등으로 구성되어 있음.

|  ID | Name      | Level | Type                  | Parent |
| --: | --------- | ----: | --------------------- | ------ |
|   1 | 소방청       |     1 | Headquarters          |        |
|   2 | 중앙소방학교    |     2 | Branch                | 1      |
|   3 | 중앙119구조본부 |     2 | Branch                | 1      |
|   4 | 국립소방연구원   |     2 | Branch                | 1      |
|   5 | 서울소방재난본부  |     2 | Regional Headquarters | 1      |
|   6 | 인천소방본부    |     2 | Regional Headquarters | 1      |
|   7 | 부산소방안본부   |     2 | Regional Headquarters | 1      |
|   8 | 대구소방안본부   |     2 | Regional Headquarters | 1      |
|   9 | 대전소본부     |     2 | Regional Headquarters | 1      |
|  10 | 광주소방안본부   |     2 | Regional Headquarters | 1      |
|  11 | 울산소방본부    |     2 | Regional Headquarters | 1      |
|  12 | 경기소방재난본부  |     2 | Regional Headquarters | 1      |
|  13 | 경기북소방재난본부 |     2 | Regional Headquarters | 1      |
|  14 | 강원소방본부    |     2 | Regional Headquarters | 1      |
|  15 | 충남소방본부    |     2 | Regional Headquarters | 1      |
|  16 | 충북소방본부    |     2 | Regional Headquarters | 1      |
|  17 | 경남방본부     |     2 | Regional Headquarters | 1      |
|  18 | 전남방본부     |     2 | Regional Headquarters | 1      |
|  18 | 전북소방본부    |     2 | Regional Headquarters | 1      |
|  20 | 제주소방안전본부  |     2 | Regional Headquarters | 1      |
|  21 | 창원소방본부    |     2 | Regional Headquarters | 1      |
|  22 | 세종방본부     |     2 | Regional Headquarters | 1      |
|  23 | 강남소방서     |     3 | Station               | 5      |
|  24 | 강동소방소     |     3 | Station               | 5      |
|  25 | 강북소방소     |     3 | Station               | 5      |
|  26 | 강서소방소     |     3 | Station               | 5      |
|  27 | 관악소방소     |     3 | Station               | 5      |
|  28 | 광진소방소     |     3 | Station               | 5      |
|  29 | 구로소방소     |     3 | Station               | 5      |
|  30 | 노원소방소     |     3 | Station               | 5      |
|  31 | 도봉소방소     |     3 | Station               | 5      |
|  32 | 동대문소방소    |     3 | Station               | 5      |
|  33 | 동작소방소     |     3 | Station               | 5      |
|  34 | 마포소방소     |     3 | Station               | 5      |
|  35 | 서대문소방소    |     3 | Station               | 5      |
|  36 | 서초소방소     |     3 | Station               | 5      |
|  37 | 성북소방소     |     3 | Station               | 5      |
|  38 | 성동소방소     |     3 | Station               | 5      |
|  39 | 송파소방소     |     3 | Station               | 5      |
|  40 | 양천소방소     |     3 | Station               | 5      |
|  41 | 영등포소방소    |     3 | Station               | 5      |
|  42 | 용산소방소     |     3 | Station               | 5      |
|  43 | 은평소방소     |     3 | Station               | 5      |
|  44 | 종로소방소     |     3 | Station               | 5      |
|  45 | 중랑소방소     |     3 | Station               | 5      |
|  46 | 중부소방소     |     3 | Station               | 5      |
|  47 | 금천소방소     |     3 | Station               | 5      |
|  48 | 영동안전센터    |     4 | Center                | 24     |
|  49 | 역삼안전센터    |     4 | Center                | 24     |
|  50 | 수서안전센터    |     4 | Center                | 24     |
|  51 | 개포안전센터    |     4 | Center                | 24     |
|  52 | 세곡안전센터    |     4 | Center                | 24     |
|     |           |       |                       |        |

# 검색

### 조건

- [Department Type (부서유형)](###부서유형)
- [Department Level (부서위계)](###부서위계)

### 연산자

- >
- <
- =
- =
- <=
### 예시

- 소방청: 소방청/전체/전체 | LV 1(=) | TY HQ
- 소방본부: 소방청/전체/전체 | LV 2(=) | TY RHQ
- 소방서: 소방청/전체/전체 | LV 3(=) | TY Station
- 안전센터: 소방청/전체/전체 | LV 4(=) | TY Center
- 특정본부 전조직: 소방청/소방본부/전체 | LV 전체(>= 2) | TY 전체
- 특정본부 소방서: 소방청/소방본부/전체 | LV 3(= 3) | TY Station
