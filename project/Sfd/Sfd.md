---
title: Smart Fire Department
Status: In Progress
tags:
  - project
  - fire
Deadline: 2024-12-31
---
# Overview

SFD is the premier CAD and RMS solution for public safety agencies. Fully integrated information management system offering you the latest technology and the most feature-rich applications since 2020. Over the years, SFD has evolved as the needs of our customers have evolved, resulting in user-friendly set of modules and features for your agency in the office and in the field.


# Solutions

### Dispatch / CAD

SFD Computer Aided Dispatch is a software suite that streamlines the process of dispatching by efficiently managing and tracking everything involved in an incident including the responding officer(s) and their status, insuring the officer's safety by making  information available such as Location History and Standard Operating Procedures, and more.

- Briareus - Material UI based WPF application.
- Cottus - Kaspersky UI based WPF application.
- Gyes - DevExpress management application UI based WPF application.

### Human Resources / HRMS

- [[Department]]
- Employee
- Position
- Rank
- Shift

### Equipment / EMS


### Report / RMS


# Build

### Tools

- Microsoft Visual Studio 2022

### Libraries

- Microsoft Dotnet 6

# Docker

1. Using Dockerfile

- [[Docker  Web]]
- [[Docker Database]]

2. Using project and publish
	- No need `Dockerfile`
	- Edit project file(.csproj) like following
```XML
<Project Sdk="Microsoft.NET.Sdk.Web">
  <PropertyGroup>
    <TargetFramework>net8.0</TargetFramework>
    <Nullable>enable</Nullable>
    <ImplicitUsings>enable</ImplicitUsings>
    <EnableSdkContainerSupport>true</EnableSdkContainerSupport>
	<PublishProfile>DefaultContainer</PublishProfile>
	<Version>0.0.1-Alpha</Version>
	<ContainerBaseImage>
		mcr.microsoft.com/dotnet/aspnet:8.0
	</ContainerBaseImage>
    <ContainerRegistry>muhayou/sfd:latest</ContainerRegistry>
    <ContainerImageTag>0.0.1-alpha</ContainerImageTag>
    <ContainerImageTags>0.0.1-alpha;latest</ContainerImageTags>
  </PropertyGroup>

  <ItemGroup>
    <ContainerPort Include="80" Type="tcp" />
    <ContainerLabel Include="certification" Value="nick-certified"/>
    <ContainerEnvironmentVariable Include="LOGGER_VERBOSITY" Value="Trace" />
  </ItemGroup>

  <ItemGroup Label="Entrypoint Assignment">
    <ContainerEntrypoint Include="dotnet" />
    <ContainerEntrypoint Include="ef" />
    <ContainerEntrypoint Include="dotnet;ef" />
  </ItemGroup>

  <ItemGroup>
    <PackageReference Include="Microsoft.EntityFrameworkCore.Design" Version="8.0.3">
      <IncludeAssets>runtime; build; native; contentfiles; analyzers; buildtransitive</IncludeAssets>
      <PrivateAssets>all</PrivateAssets>
    </PackageReference>

    <PackageReference Include="Microsoft.EntityFrameworkCore.SqlServer" Version="8.0.3" />
    <PackageReference Include="Microsoft.EntityFrameworkCore.Tools" Version="8.0.3">
      <IncludeAssets>runtime; build; native; contentfiles; analyzers; buildtransitive</IncludeAssets>
      <PrivateAssets>all</PrivateAssets>
    </PackageReference>
    <PackageReference Include="Microsoft.VisualStudio.Web.CodeGeneration.Design" Version="8.0.2" />
    <PackageReference Include="Pomelo.EntityFrameworkCore.MySql" Version="8.0.2" />
  </ItemGroup>

  <ItemGroup>
    <Folder Include="ViewModel\" />
  </ItemGroup>

  <ItemGroup>
    <ProjectReference Include="..\Sfd.Data\Sfd.Data.csproj" />
  </ItemGroup>
</Project>
```

- publish
	- `DefaultContainer` in project file is `PublishProfile`
	- `ContainerImageTag` in project file is `Version`
```PowerShell
	dotnet publish --os linux -c release -p: PublishProfile=DefaultContainer -p:ContainerImageName=muhayou/sfd -p:ContainerImageTag=0.0.1-Alpha
```

# Installation

Use the install program.

# Usage

Run the main application in windows explorer or start menu.

# Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License

[MIT](https://choosealicense.com/licenses/mit/)