### 재료

- 부추, 숙주, 당근, 양파, 양배추, 파, 식용유, 굴소스

### 조리법 

1. 대파, 양배추, 부추, 당근, 양파 등을 손질하여 준비
	* 당근과 양파는 채를 썬다
	* 파는 세로 방향으로 먼저 자른 후 가로 방향으로 잘라 준비한다
2. 팬에 식용유 1.5 스푼을 넣고 가열
3. 살짝 가열된 팬에 계란을 풀어 넣고 저어준다
4. 계란이 익으면 밥을 넣고 섞어준다
5. 파를 손질한 채소를 팬에 넣고 볶아준다
6. 채소가 적당히 볶아지면 굴소스와(1스푼) 후추 가루를 넣고 볶아준다
	* 굴소스는 반드시 채소가 볶아진 후에 넣어야 함.
7.  그릇에 담고 위에 파를 올려준다
	


** Original **: [이연복의 5분 레시피! 야채볶음밥 & 계란탕!!](https://www.youtube.com/watch?v=kCcHAeOiaRI&list=PL3YHzk2TNs8By7yy6MOUl8SA4wg94WYrV&index=102)
** Author **: [이연복](https://www.youtube.com/@leeyeonbok)

