
## Books
##### 마이크로서비스 도입, 이렇게 한다.
- 출판사: 책만
- 저자: 샘 뉴먼
- 출간: 2021. 02.20

##### 개정판 | 마이크로서비스 아키텍처 구축.
- 출판사: 한빛미디어
- 저자: 샘 뉴먼
- 출간: 2023. 06.9

## Lectures

##### [마이크로서비스 아키텍처(MSA) 오해와 진실!](https://www.youtube.com/watch?v=yDqnTBPyKhs)
- Level: 
- Platform: Youtube
- Chanel: 토크아이티(Talk IT)
- Instructors: 박준성 

##### [MSA(마이크로서비스 아키텍처) 탄생까지의 10년 역사, 10분 요약](https://www.youtube.com/watch?v=yDqnTBPyKhs)
- Level: 
- Platform: Youtube
- Channel: 토크아이티(Talk IT)
- Instructors: 박준성

##### [마이크로서비스 아키텍처의 현실적 대안, 미니서비스 아키텍처](https://www.youtube.com/watch?v=gIHx5dp_SGc&t=609s)
- Level: 
- Platform: Youtube
- Channel: 토크아이티(Talk IT)
- Instructors: 박준성

