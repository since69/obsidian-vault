## Books

##### Effective Software Testing.
- 출판사: JPUB
- 저자: 마우리시오 아니시
- 역자: 한용재
- 출간: 2023. 3. 10


## Lectures

##### [# 애자일 개발을 위한 TDD & BDD]()(https://www.youtube.com/watch?v=bRhVW8304zg)
- Level: Beginner
- Platform: Youtube
- Instructors: 박준성, 윤성열
