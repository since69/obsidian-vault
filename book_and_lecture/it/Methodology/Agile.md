
## Books
##### 헤드퍼스트 Agile.
- Level: 초중
- 출판사: 한빛미디어
- 저자: 앤드류 스텔만, 제니퍼 그린
- 출간: 2019-08-01


## Lectures

##### [# 애자일 개발을 위한 XP와 Scrum, 가장 쉬운 설명](https://www.youtube.com/watch?v=Ua-qJc7QpJk)
- Level: 
- Platform: Youtube
- Chanel: 토크아이티(Talk IT)
- Instructors: 박준성
