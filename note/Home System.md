

### Server Network

| **Host**            |   **IPv4**    | **OS**        | ID /PW              | **Description**          |
| ------------------- | :-----------: | ------------- | ------------------- | ------------------------ |
| zeus / jupiter      |               |               |                     |                          |
| hera / juno         |               |               |                     |                          |
| poseidon / neptune  |               |               |                     |                          |
| demeter / ceres     |               |               |                     |                          |
| apollo              |               |               |                     |                          |
| artemis / diana     |               |               |                     |                          |
| ares / mars         | 192.168.1.12  | Arch          |                     | Silverstone TH02         |
| athena / minerva    |               |               |                     |                          |
| hephaestus / vulcan | 192.168.1.10  | Windows  11   |                     | Lenovo Thinkstation P920 |
| aphrodite / venus   |               |               |                     |                          |
| hermes / mercury    | 192.168.1.11  | Windows 11    |                     | Alienware 17 R4          |
| dyonysus / bacchus  |               |               |                     |                          |
| hestia / vesta      |               |               |                     |                          |
|                     |               |               |                     |                          |
| argus               |               |               |                     |                          |
| hecatoncheires      |               |               |                     |                          |
| briareus            | 192.168.1.100 | Rocky         | jhkim / rlawngud@24 | Hyper-V, Dev server      |
| cottus              | 192.168.1.101 | Ubuntu v23.10 | jhkim / rlawngud@24 | Hyper-V                  |
| gyges               |               |               |                     |                          |
|                     |               |               |                     |                          |


### Application

| **Application** | **Version** | **Id**  | **Password** | **Host** | **Description** |
| --------------- | ----------: | ------- | ------------ | -------- | --------------- |
| Maria DB        |             | root    | Skrghk69     | briareus |                 |
|                 |             | sfd-adm | sfd@2024     | briareus |                 |
|                 |             | sfd-opr | sfd@2024     | briareus |                 |


