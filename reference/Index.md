---
title: OS Index
author: Joohyoung Kim
created at: 2024-02-01T03:40:00
modified at: 2024-02-01T03:40:00
---
# Business

# Dev

# Glossary

# Network

# OS

## Linux

### Install

##### Apps

-  [[neovim]]

##### OS

- [[Arch]]
- [[How I Install Arch Linux (the hard way)]]
- [[File System]]
- [[Filesystem Hierarchy]]
- [[How I Install Arch Linux (the hard way)]]
- [[How to Partitioning]]
- [[LVM]]

##### Miscs


### Command

## FreeBSD

## Windows


# Software




