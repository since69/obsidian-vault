# OS Related

### Common

- Distribution version

```Bash
lsb_release -a
```

### Arch

System update

```bash
pacman -Syu
```

### Ubuntu

- System update & upgrade
```Bash
sudo apt-get update
sudo apt-get upgrade
```
- Package search, install and remove
```Bash
apt-cache seach [package_name]
sudo apt install [package_name]
sudo apt remove [package_name]
```

```

# Miscs
### Firewall

- Check package installed

```console
rpm -qa | grep firewall
```

- Install

```console
dnf install -y firewalld
```

- Update

```console
dnf update firewalld
```

- Installed path
	- /etc/firewalld
	- /etc/firewalld/firewalld.conf   # configuration
	- /etc/firewalld/zones/public.xml    # public zone rule 

- Start / Stop / Status

```console
sudo systemctl start firewalld
sudo systemctl stop firewalld
sudo systemctl status firewalld | grep -i active
sudo firewall-cmd --state
```

- Automatic Start / Stop

```console
sudo systemctl enable firewalld
sudo systemctl disable firewalld
```

- Get service name and add by service name. (firewall role restart required)

```console
sudo firewall-cmd --get-services | grep [service-name]
sudo firewall-cmd --permanent --add-service=[service-name]
sudo firewall-cmd --reload
```

- Remove by service name. (firewall role restart required)

```console
sudo firewall-cmd --permanent --remove-service=[service-name]
sudo firewall-cmd --reload
```

- Add / Remove by port. (firewall role restart required)

```console
sudo firewall-cmd --permanent --add-port=[port_no/[tcp/udp]]
sudo firewall-cmd --permanent --remove-port==[port_no/[tcp/udp]]
sudo firewall-cmd --reload
```

- Add / Remove by ip. (But port is not allowed)

```console
sudo firewall-cmd --permanent --add-source=[ip_addr]
sudo firewall-cmd --permanent --remove-source=[ip_add]
sudo firewall-cmd --reload
```

- Add / Remove by ip range. (But port is not allowed)

```console
sudo firewall-cmd --permanent --add-source=[ip-addr/subnen-mask]
sudo firewall-cmd --permanent --add-source=[192.168.1.0/24]
sudo firewall-cmd --permanent --remove-source=[ip-add/subnet-mask]
sudo firewall-cmd --permanent --remove-source=[192.168.1.0/24]
sudo firewall-cmd --reload
```

- Add / Remove by ip and port. (If you want ip range then add subnet mask)

```console
sudo sudo firewall-cmd --permanent --add-rich-rule='rule family="ipv4" source address=[ip-addr-v4] port port="[port-no]" protocol="[tcp/udp]" accept'

sudo sudo firewall-cmd --permanent --add-rich-rule='rule family="ipv4" source address=192.168.1.100 port port="3306" protocol="tcp" accept'

sudo sudo firewall-cmd --permanent --add-rich-rule='rule family="ipv4" source address=192.168.1.0/24 port port="3306" protocol="tcp" accept'

sudo sudo firewall-cmd --permanent --remove-rich-rule='rule family="ipv4" source address=[ip-addr-v4] port port="[port-no]" protocol="tcp" accept'

sudo sudo firewall-cmd --permanent --remove-rich-rule='rule family="ipv4" source address=192.168.1.100 port port="3306" protocol="tcp" accept'

sudo sudo firewall-cmd --permanent --remove-rich-rule='rule family="ipv4" source address=192.168.1.0/24 port port="3306" protocol="tcp" accept'

sudo firewall-cmd --reload
```

- Add / Remove by ip range. (But port is not allowed)

```console
sudo firewall-cmd --permanent --add-source=[ip-addr/subnen-mask]
sudo firewall-cmd --permanent --add-source=[192.168.1.0/24]
sudo firewall-cmd --permanent --remove-source=[ip-add/subnet-mask]
sudo firewall-cmd --permanent --remove-source=[192.168.1.0/24]
sudo firewall-cmd --reload
```


- Chech all registered rules

```console
sudo firewall-cmd --list-all
```
