# Ubuntu

### Neovim

```Bash
# prepare (install required)
sudo apt-get install curl
sudo apt-get install fuse libfuse2

# download neovim binary image
curl -LO https://github.com/neovim/neovim/releases/latest/download/nvim.appimage
# add permission
chmod u+x nvim.appimage
# run for status
./nvim.appimage
# move to shared binary files dir.
sudo mv nvim.appimage /usr/local/bin/nvim

# remove image
sudo rm -R /usr/local/bin/nvim
```

### NVChad

nodejs 개발을 위한 환경 설정

- Install [[JetBrainsNerd]] font
- Install CLang
```bash
sduo apt install clang
```
- Install npm
```bash
sudo apt install npm
```
- Download NvChad
```
git clone -b v2.0 https://github.com/NvChad/NvChad ~/.config/nvim --depth 1
```
- execute & quit
- move to '~/.config/nvim'
- execute nvim
- open tree (Press Ctrl+n)
- create lua/custom/configs/lspconfig.lua and edit
```lua
local config = require("plugins.config.lspconfig)
local on_attach = config.on_attach
local capabilities = config.capabilities

local lspconfig = require("lspconfig")

lspconfig.tsserver.setup {
	on_attach = on_attach,
	capabilities = capabilities,
	init_options = {
		preferences = {
			disableSuggestions = true,
		}
	}
}
```
- create lua/custom/plugins.lua and edit
```lua
local plugins = {
	{
		"williamboman/mason.nvim",
		opts = {
			ensure_installed = {
				"typescript-language-server"
			}
		}
	},
	{
		"neovim/nvim-lspconfig",
		config = function()
			require "plugins.configs.lspconfig"
			require "custom.configs.lspconfig"
		end,
	},
}
return plugins
```
- edit lua/custom/chadrc.lua
```lua
---@type ChadrcConfig
local M = {}

M.ui = { theme = 'catppuccin' }
M.plugins = "custom.plugins"

return M
```
- save and quit
- execute MasonInstallAll in command mode
# Arch


# Rocky
