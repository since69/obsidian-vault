---
title: Logical Volume Manager
author: Joohyoung Kim
Status: In Progress
created at: 2024-02-01T03:01:00
modified at: 2024-02-01T03:01:00
---
# Logical Volume Manager

LVM 논리 볼륨의 기본 물리 저장 단위는 파티션 또는 전체 디스크와 같은 블록 장치입니다. 이 장치는 LVM _물리 볼륨_ (PV)으로 초기화됩니다.

LVM 논리 볼륨을 만들기 위해 물리 볼륨은 _볼륨 그룹_ (VG)으로 결합됩니다. 이렇게 하면 LVM 논리 볼륨(LV)을 할당할 수 있는 디스크 공간 풀이 생성됩니다. 이 프로세스는 디스크를 파티션으로 분할하는 방식과 유사합니다. 논리 볼륨은 파일 시스템 및 애플리케이션(예: 데이터베이스)에서 사용합니다.


![[lvm-key.jpeg]]

## PV, Physical Volume


## VG, Volume Group


## LV, Logical Volume


## LE, Logical Extent


## LVM Commands


### LVM Layer1

Hard Drives, Partitions and Physical Volumes

- **lvmdiskscan** - System readout of volumes and partitions
- **pvdisplay** - Display detailed info on physical volumes
- **pvscan** - Display disks with physical volumes
- **pvcreate /dev/sda1** - Create new physical volume
- **pvchange -x n /dev/sda1** - Disallow using a disk partition
- **pvresize /dev/sda1** - Resize sda1 PV to use all of the partition
- **pvresize --setphysicalvolumesize 140G /dev/sda1** - Resize sda1 to 140g
- **pvmove /dev/sda1** - Can move data out of sda1 to other PVs in VG. (*Note: Free disk space equivalent to data moved is needed elsewhere*)
- **pvremove /dev/sda1** - Delete Physical volume

### LVM Layer 2

Volume Groups

- **vgcreate vg1 /dev/sda1 /dev/sdb1** - Create a volume group form two drives
- **vgextend vg1 /dev/sdb1** - Add PV to the volume group
- **vgdisplay vg1** - Display details on a volume group
- **vgscan** - List volume groups
- **vgreduce vg1 /dev/sda1** - Removes the drive from vg1. (*Note: use pvmove /dev/sda1 before removing the drive from vg1*)
- **vgchange** - You can activate/deactive and change parameters
- **vgremove vg1** - Remove volume group vg1
- **vgsplit** and **vgmerge** Can split and merge volume groups 
- **vgrename** - Renames a volume group

### LVM Layer 3

Logical Volumes and File System

- **lvcreate -L 10G vg1** - Create a 10GB logical volume on volume group vg1
- **lvchange** and **lvreduce** are commands that typically aren't used  
- **lvrename** - Rename logical volume
- **lvremove** - Removes a logical volume
- **lvscan** - Shows logical volumes
- **lvdislplay** - Shows details on logical volumes
- **lvextend -l +100%FREE /dev/vg1/lv1** - One of the most common commands used to extend logical volume lv1 that takes up ALL of the remaining free space on the volume group vg1
- **resize2fs /dev/vg1/lv1** - Resize the file system to the size of the logical volume lv1
- **resize2fs /dev/bg1/lv1** - Resize the file system to the size of the logical volume lv1

## Usage

1. Partion 확인
	- using *lsblk*
2. [[LVM]] 설치 확인
3. Partion 수정
	- using *fdisk* or *gdisk*
4. PV 생성
	```sh
	# Check physical volumes
	pvscan
	# Create new physical volume
	pvcreate /dev/sdb1
	``` 
5. VG 생성
	```sh
	# Check volume groups
	vgscan
	# Create new volume group
	vgcreate linuxvg /dev/sdb1
	```
6. VG 확장
	```sh
	# Check volumn groups
	vgscan
	# Extend existing volume groups
	# This case extend 'linuxvg' with sdb2
	vgextend linuxvg /dev/sdb2
	```
7. LV 생성
	```sh
	# Check logical volume
	lvscan
	# Create new logical volume
	lvcreate -n swap -L 192G -C y archvg
	lvcreate -n root -l +100%FREE archvg
	```
1. File system 생성 및 Mount
2. 자동 마운트를 위해 fstab에 등록

