---
title: Linux file system
author: Joohyoung Kim
created at: 2024-02-01T02:25:00
modified at: 2024-02-01T02:25:00
---
# Linux File System
## EXT

MINIX의 파일 시스템을 보완하여 설계 되어 리눅스의 초기 파일 시스템으로 이용됨.

|                              | MINIX | EXT |
| ---------------------------- | -----:| ---:|
| Max Filename Size<br>(bytes) |    14 | 255 |
| Max File Size<br>(MB/GB)     |    64 |   2 |
## EXT2

자료 수정 타임스탬프 및 inode 수정을 지원하지 않으며, free block과 inode의 추적에 단순 linked list를 사용하여 성능이 저하되는 EXT 파일 시스템의 한계를 극복하기 위해 설계됨.

## EXT3

directory 검색 성능 향상을 위한 hash 기반 HTree 도입 및 Journaling 기능 지원.

## EXT4

파일 시스템 및 파일의 최대 크기가 작고 연속적인 데이터 블록에 대한 비효율적인 인덱싱 방식의 사용 등의 EXT3 파일 시스템의 단점을 극복하기 위해 설계 됨.

|                                | EXT3 | EXT4 |
| ------------------------------ | ----:| ----:|
| Max Filename Size<br>(TiB/EiB) |   16 |    1 |
| Max File Size<br>(TiB)         |    2 |  116 |

## XFS

1993년 **Silicon Graphics**에서 개발한 고성능 ***64bit 저널링**** 파일 시스템. 대용량 디스크의 보편화와 이에 따른 더 커진 파일 및 파일 시스템의 요구로 일부 리눅스 배포본에서 EXT4를 대체하여 기본 파일 시스템이 됨.

## Btrfs

**B-tree filesystem** 또는 **Butter filesystem**의 약자로 ***Copy-on-Write*** 파일 시스템. 2013년에 리눅스 커널에 포함되어 SUSE Linux Enterprise 12의 기본 파일 시스템으로 사용 됨.

|                              | Btrfx | EXT4 |
| ---------------------------- | -----:| ----:|
| Max Filename Size<br>(EiB)   |    16 |    1 |
| Max File Size<br>(EiB/TiB)   |    16 |   16 |
| Max Filename Size<br>(bytes) |   255 |  255 |

## ZFS, Zettabyte File System

**Sun Microsystems**에서 2006년에 개발한 ***128bit*** 파일 시스템으로 2006년에 오픈소스로 변경되어 리눅스에 포팅 됨. 
***Copy-on-Write*** 지원으로 파일 수정 시 기존 데이터를 덮어쓰지 않으며 ***Disk Mirroring, RAID-Z*** 등의 소프트웨어 RAID 제공.
Software RAID 사용으로 ***Hardware RAID Controller 사용 불가***. 