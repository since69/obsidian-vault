---
title: How I Install Arch Linux
author: Joohyoung Kim
created at: 2024-02-01T05:13:00
modified at: 2024-02-01T05:13:00
---
# How I Install Arch Linux

Arch linux install guide. Original document is [How I Install Arch Linux (the hard way)](https://www.youtube.com/watch?v=YC7NMbl4goo)

### 1. Download and Verify Files

Download using http

1. Go to arch linux [download page](https://archlinux.org/download/).
2. Choose one from the list of mirrors. (for example: [geo.mirror.pkgbuild.com](https://geo.mirror.pkgbuild.com/iso/2024.01.01/ "Download from https://geo.mirror.pkgbuild.com/"))
3. Download following files
	- [archlinux-2024.01.01-x86_64.iso](https://geo.mirror.pkgbuild.com/iso/2024.01.01/archlinux-2024.01.01-x86_64.iso)
	- [archlinux-2024.01.01-x86_64.iso.sig](https://geo.mirror.pkgbuild.com/iso/2024.01.01/archlinux-2024.01.01-x86_64.iso.sig)
	- [b2sums.txt](https://geo.mirror.pkgbuild.com/iso/2024.01.01/b2sums.txt)
	- [sha256sums.txt](https://geo.mirror.pkgbuild.com/iso/2024.01.01/sha256sums.txt)

Verify files

1. Go to the folder where the downloaded file is located.
2. b2sum check
	```sh
	b2sum -c b2sums.txt --ignore-missing
	```
3. sha256sum
	```sh
	sha256sum -c sha256sum.txt --ignore-missing
	```
4. Verify signature (required [GnuPG](https://gnupg.org/download/))
	- Download GnuPG (Ubuntu)
		```sh
		apt-get install gnupg
		```
	- Download GnuPG (Arch)
		```sh
		sudo pacman -S gnupg
		```
	- Verification
		```sh
		gpg --auto-key-locate clear,wkd -v --locate-external-key pierre@archlinux.org
		gpg verify archlinux-2024.01.01-x86_64.iso.sig
		```

### 2. Make boot disk

Make boot disk using [rufus](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=&cad=rja&uact=8&ved=2ahUKEwjrvImUxoOEAxWwcfUHHcmZCmoQFnoECAcQAQ&url=https%3A%2F%2Frufus.ie%2F&usg=AOvVaw360qlGZbGE3DcSah5A5ITM&opi=89978449) or [etcher](https://etcher.balena.io/).
If you need to copy a file, use the following command. (*/dev/disk5 is your destination partition.*)
```sh
sudo dd if=./archlinux-2-2-4.01.01-x86_64.iso of=/dev/disk5 bs=4M status=progress
```
### 3. Installation

1. Boot with boot disk
2. Set up network settings
3. Start ssh deamon
```sh
sudo systemctl start sshd
```
4. Set password (for ssh login)
```sh
passwd
```
5. Connect pc from another pc
```sh
ssh root@192.168.1.10
```
6. Check disk & Partitioning
	Create partition using *gdisk* command.
	Using *lsblk* for partitioning results.
	Following is an example using *two nvme disks*:
	- nvme0n1 (1TB)
		- efi (ef00) - EFI system partition. 512MB or 1GB
		- boot (ef02) - BIOS boot partition. 4GB
		- luks(8309) - Linux LUKS partition. Miscellaneous without /home. 927GB(remaining all space)
	- nvme1n1 (4TB)
		- linux(8302) - Linux for /home. 4TB(all space of nvme1n1)
```sh
# Check partion information
lsblk
# partitioning for /efi, /efi/boot and miscellaneous without /home
gdisk /dev/nvme0n1
# partitioning for /home
gdisk /dev/nvme1n1
```
7. LUKS (Linux Unified Key Setup)
```sh
modprobe dm-crypt
modprobe dm-mod
cryptsetup luksFormat -v -s 512 -h sha512 /dev/nvme0n1p3
cryptsetup luksFormat -v -s 512 -h sha512 /dev/nvme1n1p1
```
8. LVM
	- Decrypt partition
	- Create physical volume
	- Create volume group
	- Create logical volume for swap partition
	- Create logical volume for root partition
	- Decrypt and mount for home
	- Make efi partition
	- Make boot partition
	- Make root partition
	- Make swap partition
	- Activate swap partition
	- Mount root partition
	- Mount swap partition
	- Create home & boot directory in /mnt directory
	- Mount boot partition
	- Mount home partition
	- Create efi directory in /mnt/boot directory
	- Mount efi partition
	- Check result using *lsblk*
	- Install packages using *pacstrap*
	- Generate current mounts to *fstab* 
	- Execute *arch-chroot*
	- Install *base-devel* package
	- Install default editor(neovim)
	```sh
	cryptsetup open /dev/nvme0n1p3 luks_lvm
	pvcreate /dev/mapper/luks_lvm
	vgcreate archvg /dev/mapper/luks_lvm
	lvcreate -n swap -L 192G -C y archvg
	lvcreate -n root -l +100%FREE archvg
	cryptsetup open /dev/nvme1n1p1 arch-home
	mkfs.fat -F32 /dev/nvme0n1p1
	mkfs.ext4 /dev/nvme0n1p2
	mkfs.btrfs -L root /dev/mapper/archvg-root
	mkfs.btrfs -L home /dev/mapper/arch-home
	mkswap /dev/mapper/archvg-swap
	swapon -a
	
	mount /dev/mapper/archvg-root /mnt
	mkdir -p /mnt/{home,boot}
	mount /dev/nvme0n1p2 /mnt/boot
	mount /dev/mapper/arch-home /mnt/home
	mkdir /mnt/boot/efi
	mount /dev/nvme0n1p1 /mnt/boot/efi
	lsblk
	pacstrap -K /mnt base linux linux-firmware
	genfstab -U -p /mnt > /mnt/etc/fstab
	arch-chroot /mnt /bin/bash
	pacman -S base-devel
	pacman -S neovim
	
	```
	- *mkinitcpio.conf* 파일에 *encrypt lvm2* 추가
	```sh
	nvim /etc/mkinitcpio.conf
	# Modify this line
	HOOKS=(base udev autodetect modconf kms keyboard keymap consolefont block encrypt lvm2 filesystems fsck)
	```
	- Install lvm2 package
	```sh
	pacman -S lvm2
	```
	- Install *grub* and *efibootmgr*.
	```sh
	pacman -S grub efibootmgr
	```
	- Install boot manager
	```sh
	 grub-install --efi-directory=/boot/efi
	```
	- Modify group file
	```sh
	# ----------
	# First way
	# ----------
	
	# Open grub file
	nvim /etc/default/grub
	# Modify to the below   
	GRUB_CMDLINE_LINUX_DEFAULT="loglevel=3 quiet root=/dev/mapper/archvg-root cryptdevice=/dev/nvmen0p3:luks_lvm"

	# ----------
	# Second way
	# ----------
	
	# Open grub file
	nvim /etc/default/grub 
	# Get UUID 
	blkid /dev/nvme0n1p3
	/dev/nvme0n1p3: UUID="50b13539-4e11-be..." TYPE="crypto_LUKS" PARTLABEL="Linux LUKS" PARTUUID="9268D03a-3402-..."
	# Modify to the below
	GRUB_CMDLINE_LINUX_DEFAULT="loglevel=3 quiet root=/dev/mapper/archvg-root cryptdevice=UUID=50b13539-4e11-be...:luks_lvm"
	```
	- Create and Assign Key files 
	```sh
	# Make directory for key file
	mkdir /secure
	# Make key fire
	dd if=/dev/random of=/secure/root_keyfile.bin bs=512 count=8
	dd if=/dev/random of=/secure/home_keyfile.bin bs=512 count=8
	# Remove all permission from All user, group, others
	chmod 000 /secure/*
	# Change permission to rw
	chomod 600 /boot/initramfs-linux*
	# Assign key file to linux system partition
	cryptsetup luksAddKey /dev/nvme0n1p3 /secure/root_keyfile.bin
	Enter any existing passphrase: [Enter your password]
	# Assing key file to linux home partition
	cryptsetup luksAddKey /dev/nvme1n1p1 /secure/home_keyfile.bin
	Enter any existing passphrase: [Enter your password]
	```
	- Modify *mkinitcpio.conf* for Key file
	```sh
	nvim /etc/mkinitcpio.conf
	# Modify to the below
	FILES=(/secure/root_keyfile.bin)	
	```
	- Check /home UUID
	```sh
	blkid /dev/nvme1n1p1
	/dev/nvme1n1p1: UUID="1cdbd88c4-54ea-..." TYPE="crypto_LUKS" PARTLABEL="Linux /home" PARTUUID="e56..."
	```
	- Modify crypttab
	```sh
	# Open crypttab file
	nvim /etc/crypttab
	# Modify to the below
	arch-home    UUID=1cdbd88c4-54ea-...    /secure/home_keyfile.bin
	```
	- Image creation (Automated)
	```sh
	mkinitcpio -p linux
	grub-mkconfig -o /boot/grub/grub.cfg
	grub-mkconfig -o /boot/efi/EFI/arch/grub.cfg
	```
	- Set Localetime
	```sh
	ls /usr/share/zoneinfo
	# if Usa/Chicago
	ln -sf /usr/share/zoneinfo/America/Chicago /etc/localtime
	# if Korea
	ln -sf /usr/share/zoneinfo/Asia/Seoul /etc/localtime
	```
	- Set NTP
	```sh
	nvim /etc/systemd/timesyncd.conf
	# Modify to the below
	[Time]
	NTP=0.arch.pool.ntp.org 1.arch.pool.ntp.org 2.arch.pool.ntp.org 3.arch.pool.ntp.org
	FallbackNTP=0.pool.ntp.org 1.pool.ntp.org
	# Enable timesync service daemon
	systemctl enable systemd-timesyncd.service
	```
	- Locale
	```sh
	nvim /etc/locale.gen
	# Uncomment your locale (For example: US)
	en_US.UTF-8 UTF-8
	# Execute generate command
	locale-gen
	# Set locale
	nvim /etc/local.conf
	```

	```sh
	# Open locale file
	nvim /etc/locale.conf
	# Add to the below
	LANG=en_US.UTF-8
	```
	- Hostname
	```sh
	# Open hostname file
	nvim /etc/hostname
	# Add your host name
	hephaistus
	```
	- Set password for root
	```sh
	passwd
	New password:
	Retype new password:
	passwd: password updated successfully
	```
	- Install zsh
	```sh
	pacman -S zsh
	```
	- Add user
	```sh
	useradd -m -G wheel -s /bin/zsh jhkim
	passwd jhkim 
	New password
	Retype new password:
	passwd: password updated successfully
	```
	- sudo
	```sh
	# Set default editor to nvim
	export EDITOR=nvim
	# Config sudo env
	visudo
	# Uncomment following line
	## Uncomment to allow members of group wheel to execute any command
	%wheel ALL=(ALL:ALL) ALL
	```
	- Install *NetworkManager*
	```sh
	pacman -S networkmanager
	sudo systemctl enable NetworkManager
	```
	- Install *Microcode*
	```sh
	# For Intel CPU
	pacman -S intel-ucode
	# For AMD CPU
	pacman -S amd-ucode
	# Regenerate grub file
	grub-mkconfig -o /boot/grub/grub.cfg
	```
	- Reboot
	```sh
	# Change to root
	exit
	# Unmount all
	umount -R /mnt
	# reboot 
	reboot now
	```
	- Remove USB memory stick after restart