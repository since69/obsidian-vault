# Filesystem Hierarchy Standard

## Directories

![[Pasted image 20240205061610.png]]

| Dir   | Description                                                                                                                                                                                                                     |
| ----- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| /     | 모든 파일과 디렉토리의 시작점                                                                                                                                                                                                   |
| boot  | Boot loader, Kernel image and all required files for booting.                                                                                                                                                                    |
| bin   | Essential executables.                                                                                                                                                                                                         |
| dev   | Device files. 이 디렉터리 안에는 시스템의 하드웨어를 나타내는 파일들이 위치해 있다.                                                                                                                                             |
| etc   | ET CETERA. Editable text config.                                                                                                                                                                                                   |
| home  | All user's home directory. Contains user data files.                                                                                                                                                                                                      |
| lib   | Libraries. Shared code between binaries.                                                                                                                                                                                                                |
| media |                                                                                                                                                                                                                                 |
| mnt   | Temporary mounted file system.                                                                                                                                                                                                   |
| opt   | Optionals add on software. 추가적인 소프트웨어 패키지를 위한 디렉터리이다.                                                                                                                                                                                                                                |
| proc  |                                                                                                                                                                                                                                 |
| sbin  | System binaries. Essential executables for super user (root).                                                                                                                                                                                                   |
| sys   |                                                                                                                                                                                                                                 |
| tmp   | Temporary files. Usually deleted on boot. |
| usr   | 사용자 프로그램과 그에 관련된 데이터를 저장하는 디렉터리이다. 여기에는 또 다른 하위 디렉터리들이 있다. |
| usr/bin | Non-essential installed binaries. |
| usr/local/bin | Binaries. Locally compiled. |
| var   | Contains variable files. 시스템 운영 중에 생성되는 가변 데이터를 저장하는 디렉터리이다. 여기에는 로그 파일, 메일 큐, 출력 대기 중인 프린트 작업 등이 포함된다 |

## VAR

spool directory나 파일, 로그 데이터 그리고 임시파일 같은 **가변** 데이터 파일들이 저장된다. 그리고 /var/log, /var/run, /var/lock 과 같은 디렉토리들은 다른 시스템에서 공유가 불가능한 계층이다. /var 계층의 다른 디렉토리들은 공유가 가능한데 특히 /var/mail, /var/cache/man, /var/cache/fonts, /var/spool/news 등의 디렉토리가 그러하다.

/var 디렉토리는 /usr 디렉토리가 read-only로 마운트하도록 하는데, 시스템을 운영(설치나 유지가 아닌)하는 동안 /usr 디렉토리에 작성된 모든 것들이 /var에 있어야한다. 또한 /var 을 별도의 디렉토리로 만들 수 없는 경우, /var 디렉토리를 /usr 내부로 이동시키는 것도 좋은 방법이다.(이러한 방법은 가끔 root 계층의 사이즈를 줄이거나, 부하를 줄일 때 도움을 주기도 한다) 하지만 _**/var 디렉토리와 /usr 디렉토리가 절대 link로 연결되어선 안된다.**_ 왜냐하면 /var과 /usr 계층을 분리하기 어렵게 만들며, 네이밍 충돌이 일어나게 만들기 때문이다. 그래서 그 대안으로 /usr가 /var과 link를 걸때는 /var -> /usr/var 로 연결하곤 한다.

*/var 계층에 임의로 디렉토리를 추가하는 행위는 일반적인 행위가 아니다.*

### 주요 Sub Directory

| Dir   | Description                                 |
| ----- | ------------------------------------------- |
| cache | Cache data for Application.                 |
| lib   | 가변 상태 정보 데이터                       |
| local | /usr/local directory program 가변 데이터    |
| lock  | 잠금 파일들이 위치한다.                     |
| log   | 다양한 로그파일들이 위치한다.               |
| opt   | /opt 계층 관련 가변 데이터들이 위치한다.    |
| run   | 프로세스 운영에 관련된 데이터들이 위치한다. |
| spool | 어플리케이션 spool 데이터가 위치한다.       |
| tmp   | 재부팅 시 임시 보존되는 파일들이 위치한다.  |

#### /var/cache

/var/cache는 어플리케이션으로부터 캐시된 데이터가 저장되기 위해 존재한다. 이러한 데이터는 계산이나 다소 시간이 걸리는 입출력의 결과로 로컬에서 발생한다. 어플리케이션에서 재생성되거나 복원할 수 있어야한다. /var/spool과 달리 캐시파일들은 데이터 손실 없이 삭제 될 수 있다. /var/cache 계층에 존재하는 파일들은 시스템 관리자나, 어플리케이션의 특수한 동작으로 인해 만료될 수 있다.

#### /var/lib

해당 계층은 어플리케이션이나 시스템에 관련된 상태 정보를 수행한다. 상태 정보라 함은 프로그램이 실행되는 동한 수정하고 특정 호스트의 호출과 관련된 데이터를 말한다. 사용자는 패키지 구성을 설정을 위해서 /var/lib 계층을 변경해선 안된다. 상태 정보는 일반적으로 어플리케이션(혹은 내부적으로 연결되어 있는 어플리케이션 그룹) 의 상태를 보전하기 위해 이용된다. 상태 정보는 일반적으로 재부팅 후에도 유효해야 하며, 출력이 로깅되거나 데이터가 스풀되어선 안된다.

어플리케이션(혹은 내부적으로 연결되어있는 어플리케이션 그룹)은 /var/lib 계층을 서브디렉토리로 사용해야한다.

#### /var/local

이 계층에는 /usr/local에 연결되어있는 모든 소프트웨어의 가변 정보가 포함된다. 자연히 해당 이 디렉토리는 관리자에 의해 생성된다. 하지만 /var 계층의 다른 범주에 포함되는 정보들은/var/local 디렉토리에 존재하면 안된다. 예를 들어 lock 파일들은 /var/lock 에 저장되어야만 한다.

#### /var/lock

lock files는 /var/lock 디렉토리에 존재해야한다.

디바이스나 여러 어플리케이션들에 의해 공유되는 리소스에 대한 lock files는 과거에는 /var/spool/locks 나 /var/spool/uucp 들에 저장되곤 했다. 하지만 이제는 /var/lock에 저장된다. lock files에 대한 네이밍 컨벤션은 디바이스 이름 앞에 'LCK..'을 붙여 사용해야 한다. 예를 들어 /dev/ttyS0의 lock은 'LCK..ttyS0'로 생성되어야 한다.

#### /var/log

해당 계층에는 다양한 종류의 로그 파일들이 저장된다. 거의 대부분의 로그 파일들은 이 디렉토리에 작성되어야한다.

#### /var/opt

/opt 디렉토리 안에 있는 모든 패키지에 대한 가변 데이터는 /var/opt/에 설치되어야한다. 여기서 은 /etc에서 다른 파일로 대체된 경우를 제외하고 /opt 트리 구조를 따르는 이름으로 설정해야한다.

#### /var/run

시스템이 부팅된 이후에 기술되는 시스템 정보 데이터를 포함한다. 이 디렉토리에 존재하는 파일들은 시스템이 부팅될 때 반드시 삭제되어야 한다. /var/run 계층에 서브디렉토리를 갖고 있는 프로그램들은 하나 이상의 run-time 파일을 갖고 있는 것이다. 원래 /etc에 존재했던 pid 파일들은 /var/run 에 네이밍 컨벤션은 /var/run/<program-name.pid> 이다. 예를 들어 crond 의 pid 파일은 /var/run/crond.pid 이다.

> *** PID ***
> 
> PID의 내부 포맷은 변하지 않는데, PID의 형식은 반드시 ASCII로 인코딩된 십진법으로 표기되어야 하며 맨 마지막에는 newline이 포함되어야 한다. PID를 읽는 프로그램들은 몇 가지 예외 사항에 대응할 수 있어야하는데, 대표적으로 PID파일에 newline이 없거나 whitespace 등을 무시할 수 있어야한다. 또한 PID를 만들어내는 프로그램들은 앞서 설명했던 네이밍 컨벤션을 사용해야한다.

#### /var/spool

/var/spool 계층에는 나중에 처리되기 위해 대기 중인 데이터들을 포함한다. 해당 계층에 존재하는 데이터들은 대부분 시스템 사용자나 관리자, 프로그램 등에 의해 처리될 작업들을 나타낸다. 물론 작업이 처리되기 시작하면 삭제되는 경우가 많다.

#### /var/tmp

/var/tmp 계층에는 시스템이 재부팅되는 동안 보존되는 임시파일들이 위치한다. 해당 계층에 있는 모든 파일과 디렉토리들은 시스템이 재부팅됨과 동시에 삭제되어야한다.