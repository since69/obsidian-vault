## Prepare

When the installation media runs and a prompt appears, execute the contents below. 

- Change terminal font and archlinux repository sync.

``` console
setfont ter-v28n
pacman -Sy
```

- Run installer

``` consol
archinstall
```

- Install with minimum profile

```script

#!/bin/bash

# get password
echo -n "Enter your password: "
read -s password

# update archlinux repository
password | sudo pacman -Syu

# install font, relative tools and change font 
echo "y" | sudo pacman -S fontconfig
echo "y" | sudo pacman -S terminus-font
echo "y" | sudo pacman -S ttf-terminus-nerd
#echo "y" | sudo pacman -S ttf-font-awesome
echo "y" | sudo pacman -S ttf-jetbrains-mono-nerd
setfont ter-v28n

# install required packages
echo "y" | sudo pacman -S --needed base-devel git
cd /opt
sudo git clone https://aur.archlinux.org/paru-git.git ./paru
sudo chown -R $USER:$USER paru
cd paru
makepkg -si
paru
cd /opt
sudo git clone https://aur.archlinux.org/brave-bin.git ./brave
sudo chown -R $USER:$USER brave
cd brave
makepkg -si
cd $HOME
echo "y" | sudo pacman -S zsh
echo "y" | sudo pacman -S neofetch
# desktop manager
echo "y" | sudo pacman -S hyprland
# terminal emulator
echo "y" | sudo pacman -S kitty
# terminal
echo "y" | sudo pacman -S wezterm
# desktop header bar
echo "y" | sudo pacman -S waybar
# lockscreen manager
echo "y" | sudo pacman -S swaylock
# notify daemon
echo "y" | sudo pacman -S dunst
# launcher
echo "y" | sudo pacman -S rofi
# file manager
echo "y" | sudo pacman -S ranger
# prompt decoration
echo "y" | sudo pacman -S starship
# fast node manager
echo "1" | paru -S fnm

# prepare to install hyprland
author="linuxmobile"
basedir="$HOME/Documents/dotfiles/hypr"
srcdir="$basedir/$author"
giturl="https://github.com/linuxmobile/hyprland-dots.git"
mkdir -p $basedir
git clone $giturl $srcdir

```

- Enable hyper-v enhanced session (Optional)

```coneole
git clone https://github.com/microsoft/linux-vm-tools ~/repos/microsoft/linux-vm-tools
cd ~/repos/microsoft/linux-vm-tools/arch
chmod +x makepkg.sh install-config.sh
makepkg.sh
install-config.sh
```

```PowerShell
Set-Vm -VMName arch -EnhancedSessionTransportType HvSocket
```

