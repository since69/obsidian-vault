---
title: How I Install Arch Linux
author: Joohyoung Kim
created at: 2024-03-20T06:44:00
modified at:
---
# How I Install Arch Linux


## Download and Verify Files

1. Go to arch linux [download page](https://archlinux.org/download/).
2. Choose one from the list of mirrors. (for example: [geo.mirror.pkgbuild.com](https://geo.mirror.pkgbuild.com/iso/2024.01.01/ "Download from https://geo.mirror.pkgbuild.com/"))
3. Download following files
	- [archlinux-2024.01.01-x86_64.iso](https://geo.mirror.pkgbuild.com/iso/2024.01.01/archlinux-2024.01.01-x86_64.iso)
	- [archlinux-2024.01.01-x86_64.iso.sig](https://geo.mirror.pkgbuild.com/iso/2024.01.01/archlinux-2024.01.01-x86_64.iso.sig)
	- [b2sums.txt](https://geo.mirror.pkgbuild.com/iso/2024.01.01/b2sums.txt)
	- [sha256sums.txt](https://geo.mirror.pkgbuild.com/iso/2024.01.01/sha256sums.txt)
 4. Go to the folder where the downloaded file is located. And b2sum, sha256sum check.
  ```bash
  b2sum -c b2sums.txt --ignore-missing
  sha256sum -c sha256sum.txt --ignore-missing
  ```
 5. Verify signature (required [GnuPG](https://gnupg.org/download/))
 ```bash
 # Install GnuPG in Ubunt
 apt-get install gnupg
 # Install GnuPG in Arch
 sudo pacman -S gnupg
 # Verification
 gpg --auto-key-locate clear,wkd -v --locate-external-key pierre@archlinux.org
		gpg verify archlinux-2024.01.01-x86_64.iso.sig
 ```


## Make boot disk

Make boot disk using [rufus](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=&cad=rja&uact=8&ved=2ahUKEwjrvImUxoOEAxWwcfUHHcmZCmoQFnoECAcQAQ&url=https%3A%2F%2Frufus.ie%2F&usg=AOvVaw360qlGZbGE3DcSah5A5ITM&opi=89978449) or [etcher](https://etcher.balena.io/).
If you need to copy a file, use the following command. (*/dev/disk5 is your destination partition.*)
```bash
sudo dd if=./archlinux-2-2-4.01.01-x86_64.iso of=/dev/disk5 bs=4M status=progress
```


## Installation

##### Boot and connect from remote computer with SSH

- Boot with boot disk
- Set up network settings
- Start ssh daemon
```bash
sudo systemctl start sshd
```
- Set password(using *passwd* command) for ssh login
- Connect from another computer
```bash
ssh root@192.168.1.10
```

#### Prepare disk 

Three 128GB Samsung SSD. Using LVM.

| Disk             | Name | Partion | Type  | Code | Size | Mount         |
| ---------------- | ---- | ------- | ----- | ---- | ---: | ------------- |
| 128G Samsung SSD | sda  | sda1    | efi   | ef00 |   1G | /mnt/boot/efi |
|                  |      | sda2    | boot  | ef02 |   4G | /mnt/boot     |
|                  |      | sda3    | luks  | 8e00 | 123G |               |
| 128G Samsung SSD | sdb  | sdc1    | luks  | 8e00 | 128G |               |
| 128G Samsung SSD | sdc  | sdd1    | linux | 8e00 | 128G | /mnt/home     |

#### Install OS

```bash
#
# create partitions
#
# sda1 for /boot/efi
# sda2 for /boot
# sda3, sdb1 for /root
# sdd1 for /home
gdisk /dev/sda
gdisk /dev/sdb
gdisk /dev/sdd

#
# create LVM volumes
#
# create physical volumes
pvcreate /dev/sda3
pvcreate /dev/sdb1
pvcreate /dev/sdd1
# create volume groups
vgcreate arch_vg /dev/sda3 /dev/sdb1
vgcreate home_vg /dev/sdd1
# create logical volumes
lvcreate -n swap_lv -L 32G -C y arch_vg
lvcreate -n root_lv -l +100%FREE arch_vg
lvcreate -n home_lv -l +100%FREE home_vg

#
# format volumes
#
# init efi partion
mkfs.fat -F32 /dev/sda1
# init boot partion
mkfs.ext4 /dev/sda2
# init swap, root and home partion
mkfs.ext4 -L swap /dev/arch_vg/swap_lv
mkfs.ext4 -L root /dev/arch_vg/root_lv
mkfs.ext4 -L home /dev/home_vg/home_lv

# enable swap
mkswap /dev/arch_vg/swap_lv
swapon -a

#
# mount partion
#
mount /dev/arch_vg/root_lv /mnt
mkdir -p /mnt/{boot,home}
mount /dev/sda2 /mnt/boot
mkdir /mnt/boot/efi
mount /dev/sda1 /mnt/boot/efi
mount /dev/home_vg/home_lv /mnt/home

# if wiress netwokr need config
- write "iwctl". this will give a prompt for the network interface 
- write "station list" will give a list of all the stations aka simila6to ifconfig in debian ( provides the eth0, wlan0 and bla bla bla) 
- step3: write "station wlan0 get-networks" i.e search for the network using the wlan0 
- step4: look for the wifi ssd that resembles yours. 
- step5: after choosing the ssid, write " station wlan0 connect SSID_NAME" 
- step6: it will ask for a password or passphrase (yep password) and there you go and remember CTRL+C with checking like does the internet work (use "ping [www.cornhub.com](http://www.cornhub.com)") xd have fun (T_T i still have to do a bunch of stufff)

# unpack required packages
pacstrap -K /mnt base linux linux-firmware 

# generate fstab
genfstab -U -p /mnt > /mnt/etc/fstab

# default shell
arch-chroot /mnt /bin/bash

# install required package
pacman -S base-devel
pacman -S neovim
pacman -S lvm2
pacman -S net-tools
pacman -S zsh

#
# Adding mkinitcpio hooks
# 
# open mkinitcpio.conf
nvim /etc/mkinitcpio.conf
# insert lvm2
...
HOOKS=(base udev ... block lvm2 filesystems)
...

# install grub efibootmgr
pacman -S grub efibootmgr

# install boot manager
grub-install --efi-directory=/boot/efi

# modify grub file. for lvm root partition.
nvim /etc/default/grub
# add root partition location.
...
GRUB_CMDLINE_LINUX_DEFAULT="loglevel=3 quiet root=/dev/arch_vg/root_lv"
...

# create boot image
pacman -S linux
mkinitcpio -p linux
grub-mkconfig -o /boot/grub/grub.cfg
grub-mkconfig -o /boot/efi/EFI/arch/grub.cfg

# set localetime
ls /usr/share/zoneinfo
ln -sf /usr/share/zoneinfo/Asia/Seoul /etc/localtime

# set NTP
nvim /etc/systemd/timesyncd.conf
# Modify to the below
[Time]
NTP=0.arch.pool.ntp.org 1.arch.pool.ntp.org 2.arch.pool.ntp.org 3.arch.pool.ntp.org
FallbackNTP=0.pool.ntp.org 1.pool.ntp.org
# Enable timesync service daemon
systemctl enable systemd-timesyncd.service

#
# set Locale
#
# open locale.gen
nvim /etc/locale.gen

	# Uncomment your locale (For example: US)
	en_US.UTF-8 UTF-8
	ko_KR.UTF-8 UTF-8
	# Execute generate command
	locale-gen
	
# Open locale file
nvim /etc/locale.conf
	# Add to the below
	LANG=en_US.UTF-8

#
# set hostname
#
# Open hostname file
nvim /etc/hostname
# Add your host name
hephaistus

# set root password
passwd
New password:
Retype new password:
passwd: password updated successfully

# add user
useradd -m -G wheel -s /bin/zsh jhkim
passwd jhkim 
New password
Retype new password:
passwd: password updated successfully

# sudo
# Set default editor to nvim
export EDITOR=nvim
# Config sudo env
visudo
# Uncomment following line
## Uncomment to allow members of group wheel to execute any command
%wheel ALL=(ALL:ALL) ALL

# install network manager
pacman -S networkmanager
systemctl enable NetworkManager

#install Microcode
 # For Intel CPU
pacman -S intel-ucode
# For AMD CPU
pacman -S amd-ucode
# Regenerate grub file
grub-mkconfig -o /boot/grub/grub.cfg

# reboot
# Change to root
exit
# Unmount all
umount -R /mnt
# reboot 
reboot
```

#### Install Gnome

```bash
sudo pacman -Syu
sudo reboot
sudo pacman -S xorg xorg-server
sudo pacman -S gnome
sudo systemctl enable gdm.service
sudo systemctl start gdm.service
sudo reboot
```

#### Install Hyprland

```bash
# prepare
sudo pacman -Ss python
sudo pacman -Ss python-pip
# install paru
git clone https://aur.archlinux.org/paru.git
cd paru
makepkg -si
# update arch linux (Equivalent to 'pacman -Syu' command)
paru


# hyprland stuff
# hyprpicker-git (X)
## Hyprland Stuff
paru -S hyprland-git hyprpicker-git waybar-git \
dunst nwg-look wf-recorder wlogout wlsunset

# dependencies
# xwaylandvideobridge-cursor-mode-2-git (x)
# replace to xwaylandvideobridge-git
paru -S colord ffmpegthumbnailer gnome-keyring grimblast-git gtk-engine-murrine \
imagemagick kvantum pamixer playerctl polkit-kde-agent qt5-quickcontrols        \
qt5-quickcontrols2 qt5-wayland qt6-wayland swww ttf-font-awesome tumbler     \
ttf-jetbrains-mono ttf-jetbrains-mono-nerd ttf-icomoon-feather \
xdg-desktop-portal-hyprland-git xdotool  \
xwaylandvideobridge-cursor-mode-2-git cliphist qt5-imageformats qt5ct

# tmeme
# catppuccin-gtk-theme-mocha (x)
paru -S catppuccin-gtk-theme-macchiato catppuccin-gtk-theme-latte catppuccin-gtk-theme-frappe papirus-icon-theme sddm-git swaylock-effects-git kvantum kvantum-theme-catppuccin-git

# apps & more
paru -S btop cava neofetch noise-suppression-for-voice   \
rofi-lbonn-wayland-git rofi-emoji starship viewnior ocs-url
paru -S file-roller thunar thunar-archive-plugin

# audio (pipewire)
paru -S pipewire pipewire-alsa pipewire-audio \
pipewire-pulse pipewire-jack wireplumber gst-plugin-pipewire \ 
pavucontrol

# dotfiles
sudo pacman -S rsync
mkdir -p ~/repos/github/linuxmobile
cd ~/repos/github/linuxmobile
git clone https://github.com/linuxmobile/hyprland-dots
rsync -avxHAXP --exclude '.git*' .* ~/
```

#### Install Miscs 

```bash
sudo pacman -S noto-fonts noto-fonts-cjk noto-fonts-emoji
sudo pacman -S firefox
paru -S code code-features code-marketplace
```