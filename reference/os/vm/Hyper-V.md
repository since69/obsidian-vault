
# Commands


- Get a list of running integration services

``` powershell
Get-VMIntegrationService -VMName "Name of VM in Manager"
``` 

- Adjust screen resoultion of guest os console

``` powershell
Set-VMVideo -VMName "Name of VM in Manager" -HorizontalResolution 1920 -VerticalResolution 1200 -ResolutionType Single
``` 