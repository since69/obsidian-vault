
### Profile

##### Profiles
| **Profile** | **Command** | **Path** |
| ---- | :--- | ---- |
| Current User - Current Host | $profile | $HOME\[My ]Documents\PowerShell\Microsof.PowerShell_profile.ps1 |
| Current User - All Hosts | $profile.CurrentUserAllHosts | $HOME\[My ]Documents\PowerShell\Profile.ps1 |
| All Users - Current Host | $profile.AllUsersCurrentHost | $PSHOME\Microsoft.PowerShell_profile.ps1 |
| All Users - All Hosts | $profile.AllUsersAllHosts | $PSHOME\Profile.ps1 |


##### User profile

- Open terminal
- Create folder and profile

```PowerShell
mkdir $env:USERPROFILE\.config\powershell
```

- Create profile (Use editor) 

```PowerShell
nvim $env:USERPROFILE\.config\powershell\user_profile.ps1
```

- Set profile for CurrentUserCurrentHost

```PowerShell
nvim $PROFILE.CurrentUserCurrentHost
```

- Add following line

```
. $env:USERPROFILE\.config\powershell\user_profile.ps1
```

- Force Create profile 

```
New-Item -Path $PROFILE -ItemType File -Force
```

- Remove OneDrive reference
```
```bash
# change
Set-ItemProperty -Path "HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\User Shell Folders" -Name "Personal" -Value "$env:UserProfile\Documents"
# confirm
```bash
Get-ItemProperty -Path "HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\User Shell Folders" -Name "Personal"
```

### Themes

##### Catppucchin
- [Repository](https://github.com/catppuccin/powershell)
- [PowerShell Prompt](https://learn.microsoft.com/en-us/powershell/module/microsoft.powershell.core/about/about_prompts?view=powershell-7.4)
- [Terminal Color Preset](https://github.com/catppuccin/windows-terminal)

### Install Modules

##### Terminal Icon
``` Powershell
Install-Module -Name Terminal-Icons -Repository PSGallery -Force
```
Add to user_profile.ps1
```
Import-Module -Name Terminal-Icons
```

### Get Module Path

```PowerShell
$env:PSModulePath
```

### UserProfile

```Profile
# user_profile

# Imports
Import-Module -Name Terminal-Icons

# aliases
Set-Alias g git
Set-Alias vim nvim
# Set-Alias commit "git add .; git commit -m"
# Set-Alias push "git push"
# Set-Alias pull "git pull"

# git functions
function commit { git add .; git commit -m $args }
function push { git push }
function pull { git pull }
# cd functions
function cdr { Set-Location "e:\repos" }
function cdgitlab { Set-Location "e:\repos\gitlab" }
function cdsince69 { Set-Location "e:\repos\gitlab\since69" }
function cdsfd { Set-Location "e:\repos\gitlab\since69\sfd" }
function cdsfd-db { Set-Location "e:\repos\gitlab\since69\sfd\db" }
function cdsfd-webapp { Set-Location "e:\repos\gitlab\since69\sfd\webapp" }
function cdfms { Set-Location "e:\repos\gitlab\since69\fms" }
function cdfms-db { Set-Location "e:\repos\gitlab\since69\fms\db" }
function cdobsidian { Set-Location "e:\repos\gitlab\since69\obsidian-vault" }
function cdsandbox { Set-Location "e:\repos\gitlab\since69\sandbox" }
function cdsandbox-java { Set-Location "e:\repos\gitlab\since69\sandbox\java" }

# Invokes
Invoke-Expression (&starship init powershell)
```

### Starship

`~\.config\starship.toml`

```Profile
## ░█▀▀░▀█▀░█▀█░█▀▄░█▀▀░█░█░▀█▀░█▀█
## ░▀▀█░░█░░█▀█░█▀▄░▀▀█░█▀█░░█░░█▀▀
## ░▀▀▀░░▀░░▀░▀░▀░▀░▀▀▀░▀░▀░▀▀▀░▀░░

# Sets user-defined palette
palette = "catppuccin_mocha"

format = """
$directory\
$git_branch\
$fill\
$git_status\
$cmd_duration\
$line_break\
$character"""

[character]
success_symbol = "[❯](bold green)"
error_symbol = "[>](bold red)"
#vimcmd_symbol = "[❮](green)"
vicmd_symbol = "[](peach)"

[directory]
format = "[]($style)[ ](bg:base fg:pink)[$path](bg:base fg:lavender bold)[ ]($style)"
read_only = "🔒"
read_only_style = "red"
truncation_length = 3
truncation_symbol = ".../"
truncate_to_repo=false
style = "bg:noe fg:base"

[fill]
symbol = ' '
disabled = false

[git_branch]
#symbol = "🌱 "
format = "[]($style)[[ ](bg:base fg:green bold)$branch](bg:base fg:lavender)[ ]($style)"
style = "bg:none fg:base"

[git_status]
# $all status$ahead_behind
format = "[]($style)[$all_status$ahead_behind](bg:base fg:yellow)[]($style)"
style = "bg:none fg:base"
conflicted = "="
ahead =	"⇡${count}"
behind = "⇣${count}"
diverged = "⇕⇡${ahead_count}⇣${behind_count}"
up_to_date = ""
untracked = "?${count}"
stashed = " "
modified = "!${count}"
#modified = " ${count}"
staged = "+${count}"
renamed = "»${count}"
deleted = " ${count}"

[line_break]
disabled = false

#[time]
#time_format = "%T"
#format = "🕙[\\[$time\\]]($style) "
#disabled = false

[cmd_duration]
min_time = 1
# duration & style 
format = "[]($style)[[ ](bg:base fg:flamingo bold)$duration](bg:base fg:text)[ ]($style)"
disabled = false
style = "bg:none fg:base"

# palette tables should be last in the config ⚓️
[palettes.catppuccin_macchiato]
rosewater = "#f4dbd6"
flamingo = "#f0c6c6"
pink = "#f5bde6"
mauve = "#c6a0f6"
red = "#ed8796"
maroon = "#ee99a0"
peach = "#f5a97f"
yellow = "#eed49f"
green = "#a6da95"
teal = "#8bd5ca"
sky = "#91d7e3"
sapphire = "#7dc4e4"
blue = "#8aadf4"
lavender = "#b7bdf8"
text = "#cad3f5"
subtext1 = "#b8c0e0"
subtext0 = "#a5adcb"
overlay2 = "#939ab7"
overlay1 = "#8087a2"
overlay0 = "#6e738d"
surface2 = "#5b6078"
surface1 = "#494d64"
surface0 = "#363a4f"
base = "#24273a"
mantle = "#1e2030"
crust = "#181926"

[palettes.catppuccin_frappe]
rosewater = "#f2d5cf"
flamingo = "#eebebe"
pink = "#f4b8e4"
mauve = "#ca9ee6"
red = "#e78284"
maroon = "#ea999c"
peach = "#ef9f76"
yellow = "#e5c890"
green = "#a6d189"
teal = "#81c8be"
sky = "#99d1db"
sapphire = "#85c1dc"
blue = "#8caaee"
lavender = "#babbf1"
text = "#c6d0f5"
subtext1 = "#b5bfe2"
subtext0 = "#a5adce"
overlay2 = "#949cbb"
overlay1 = "#838ba7"
overlay0 = "#737994"
surface2 = "#626880"
surface1 = "#51576d"
surface0 = "#414559"
base = "#303446"
mantle = "#292c3c"
crust = "#232634"

[palettes.catppuccin_latte]
rosewater = "#dc8a78"
flamingo = "#dd7878"
pink = "#ea76cb"
mauve = "#8839ef"
red = "#d20f39"
maroon = "#e64553"
peach = "#fe640b"
yellow = "#df8e1d"
green = "#40a02b"
teal = "#179299"
sky = "#04a5e5"
sapphire = "#209fb5"
blue = "#1e66f5"
lavender = "#7287fd"
text = "#4c4f69"
subtext1 = "#5c5f77"
subtext0 = "#6c6f85"
overlay2 = "#7c7f93"
overlay1 = "#8c8fa1"
overlay0 = "#9ca0b0"
surface2 = "#acb0be"
surface1 = "#bcc0cc"
surface0 = "#ccd0da"
base = "#eff1f5"
mantle = "#e6e9ef"
crust = "#dce0e8"

[palettes.catppuccin_mocha]
rosewater = "#f5e0dc"
flamingo = "#f2cdcd"
pink = "#f5c2e7"
mauve = "#cba6f7"
red = "#f38ba8"
maroon = "#eba0ac"
peach = "#fab387"
yellow = "#f9e2af"
green = "#a6e3a1"
teal = "#94e2d5"
sky = "#89dceb"
sapphire = "#74c7ec"
blue = "#89b4fa"
lavender = "#b4befe"
text = "#cdd6f4"
subtext1 = "#bac2de"
subtext0 = "#a6adc8"
overlay2 = "#9399b2"
overlay1 = "#7f849c"
overlay0 = "#6c7086"
surface2 = "#585b70"
surface1 = "#45475a"
surface0 = "#313244"
base = "#1e1e2e"
mantle = "#181825"
crust = "#11111b"

```