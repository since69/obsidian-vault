
# Cost Accounting Defined: What It Is & Why It Matters

The ability to control costs is a business fundamental. Perhaps nowhere is that more evident than in the production of goods and delivery of services, processes with myriad expenses that if not closely tracked can easily eat into or wipe out a company’s profit margin.

Cost accounting helps protect margins by organizing and tracking all direct and indirect expenses, providing important insights that can lead to better budgeting, increased efficiency and, ultimately, higher profit.


### Waht Is Cost Accounting?

Cost accounting analyzes a company’s total production costs for its products or services. A form of management accounting, cost accounting examines all variable and fixed expenses and is meant for internal eyes only. Company decision-makers use the results to identify which products and services are most profitable and which ones cost too much to produce relative to sales.

Cost accounting informs budgeting decisions, product/service pricing and business strategy.


### Cost Accounting Explained

Cost accounting is the process of tracking, analyzing and summarizing all fixed and variable “input” costs related to the production of a product, acquisition of goods for sale or the delivery of a service. These include material and labor costs, as well as operating costs associated with a product or service. Cost accounting helps companies identify areas where they may be able to better control their costs, and also informs pricing decisions to ensure profitability.

Cost accounting figures are used only by a company’s internal management team, so collection methods can be customized according to company needs. 


### Cost Accounting s Financial Accounting

Cost accounting details the costs associated with producing or acquiring goods for sale or providing a service. Because it’s not mandatory to perform, cost accounting is not bound to the same standards required of financial accounting to meet the requirements of external parties.

This table presents a side-by-side comparison of each form of accounting:

|  **Cost Accounting** | **Financial Accounting** |
|---|---|
| Organizes and analyzes costs to facilitate cost control and efficiency improvements. | Organizes and records a company’s financial transactions. |
| Reports only to internal management. | Reports the financial position of the company to shareholders, creditors, the government (including tax agencies), investors and external analysts. |
| Can be organized according to the needs of management and the characteristics of the business. | Must conform to accounting standards such as GAAP and IFRS. |
| Deals with objective, cost-related data requiring managerial judgment for allocating expenses. |Aims to present an objective (“true and fair”) view of the company’s finances. |


### What Is the Prupose of Cost Accounting?

Cost accounting helps organizations evaluate the costs associated with manufacturing a product or providing a service. While the process itself requires a considerable level of detail and time, the strategic insights gained make it a worthwhile endeavor for most any organization.

Among the areas where cost accounting can help:

**Budgeting:** Cost accounting is at the heart of [budget planning](https://www.netsuite.com/portal/resource/articles/financial-management/budget.shtml). By analyzing actual expenses, an organization can more accurately estimate future fixed and variable costs and allocate them to product lines.

**Efficiency:** Standard costs are based on the efficient use of labor and materials. Cost accounting gives managers a bird’s-eye view of how closely (or not) budgeted costs match actual costs.

**Profit:** Uncontrolled variations in expenses can diminish or eliminate profits even if sales are strong. Cost accounting pinpoints when and where specific production expenses begin to outweigh sales, enabling managers to make adjustments.


### Elements of Cost Accounting

Cost accounting is based on three principal elements: materials, labor and overhead.

#### Material

Materials are inputs to production. They are typically broken down into two groups: direct and indirect.

**Direct materials** are materials and parts used in production and reflected in a completed product. Materials can be subdivided into raw materials, such as cotton for clothes or plastic for a phone case; work-in-progress, or products that are not yet complete; and finished goods, meaning products that are ready for sale.

**Indirect materials** are treated as an overhead expense. Examples include safety equipment and cleaning supplies. Only direct materials are shown on the cost sheet.

#### Labor

Workers directly involved in production or distribution of goods or delivery of services must be paid. Their salaries or wages might include overtime and bonuses; employee benefits are part of the total cost, too.

As with indirect materials, indirect labor costs are treated as an overhead expense, not a labor expense.

#### Expenses/overhead

These are costs related to the production or distribution of goods or provision of services, but which cannot be directly attributed to specific goods or services. Typical overhead costs include:

-   Equipment set up, such as for factory machinery.
-   Utility bills, such as factory electricity, water and sewerage.
-   Facilities costs, including rent/mortgage and property taxes.
-   Payroll taxes and pension contributions.
-   Depreciation of fixed assets, such as factory machinery and store equipment.
-   Interest payments.


### Const Accounting Systems

A cost accounting system helps determine how much the production of a good or service will cost. There are two types of systems: job order costing and process costing.

[Job order costing](https://www.netsuite.com/portal/resource/articles/accounting/job-costing.shtml) is typically used by businesses with diversified or customizable products, or by companies that provide services, where labor is the dominant expense — for example, a specialist furniture manufacturer or a plumbing or electrical services provider.

In other words, no two jobs are exactly the same. Job order costing estimates and tracks costs for direct materials, labor and overhead costs.

[Process costing](https://www.netsuite.com/portal/resource/articles/accounting/process-costing.shtml) is often the better choice for firms that mass-produce standardized products. Instead of estimating the cost of each item involved in the production process, process costing assumes the unit cost of each item is the same and allocates production costs evenly across the company’s entire output.


### Types of Costs

The production of goods and services involves several types of costs. It is important for businesses to understand them — and include them in their cost accounting calculations — to better control their expenses and improve operational efficiency.

**Direct costs** are related to the production/acquisition of products or delivery of services. For a manufacturer, these would include the raw materials and parts that go into a final product, as well as the labor involved in its production. These are also known as product costs. For some services-based businesses, such as a law firm, labor may be the only direct cost. Others, such as an auto mechanic, require inventory — car parts, for example — to perform services, so that counts as a direct cost.

**Operating costs** are indirect costs related to production that cannot be tied to a specific product or service. Heating and lighting are all examples of indirect costs, as is the labor behind them. Equipment purchases are also indirect costs because, while used for production, they don’t go into the final product. That applies to services-based businesses, too. For example, hairdressers must purchase scissors and hairdryers, but unless clients take them home after a haircut, they are an indirect cost.

**Fixed costs** don’t change with production and have to be paid regardless of the level of production; when production or demand for a product falls, fixed costs cause unit costs to rise, and vice versa.

**Variable costs** fluctuate with a company’s level of production. A manufacturer of skiing equipment is likely to see its costs for materials, labor and overhead rise, and fall in the spring and summer. Some costs have both fixed and variable components. For example, the cost of electricity to run production machinery varies with usage, but the cost of electricity to heat and light the building generally doesn’t unless a company adds shifts, for example.


### Types of Cost Accounting

There are many different types of cost accounting, each with its own focus and approach to analyzing production expenses. Following is an explanation of each.

**Standard costing:** Standard costing estimates costs based on the most efficient use of labor and materials under typical operating conditions. When production is completed, actual costs are compared with estimated costs.

The resulting variance highlights the difference between the two and can be key to effective cost control. For example, if actual costs are persistently higher than standard costs, then management might consider renegotiating supplier contracts, improving business processes or making other changes to bring production costs down. Variances may also indicate that assumptions made when estimating standard costs need to be revisited.

**Activity-based costing (ABC):** This form of cost accounting identifies and allocates overhead costs to the activities involved with producing a good or service. [ABC costing](https://www.netsuite.com/portal/resource/articles/inventory-management/abc-inventory-analysis.shtml) is particularly useful when a company has diverse product lines involving different levels of material or labor.

Take, for example, a ceramics manufacturer that produces two types of patterned plates. The production of one plate is entirely automated; production for the second type involves some time-consuming manual work, which the company will want reflected in the unit price. A standard costing approach would allocate production costs evenly across both lines, resulting in an overstated production cost for the first type of plate and an understated cost for the second type. The ABC approach would allocate a higher proportion of labor costs to the second product line, thus giving a more accurate picture of the cost distribution of the two lines.

### Related accounting approaches and disciplines

**Lean accounting:** Lean accounting supports “lean” thinking: streamlining production and eliminating waste to maximize productivity. It tries to avoid overproduction by running down inventories and creating [just-in-time supply lines](https://www.netsuite.com/portal/resource/articles/inventory-management/just-in-time-inventory.shtml). The goal is to meet customer demand, rather than production targets. Lean cost accounting focuses on value streams, which are sets of actions that deliver value that customers are willing to pay for. It doesn’t distinguish between direct and indirect costs — all costs associated with a value stream are regarded as direct costs. Value stream costs include labor, materials, production support, machines and equipment, operation support, facilities and maintenance. Costs that don’t fall into the value stream are separately grouped as “business sustaining costs.”

Because lean doesn't capture all costs related to production, it isn't as useful for pricing as cost accounting. As a result, many companies use both approaches.

**Project accounting:** This is a separate discipline that focuses on the financial transactions related to managing a project, including project costs, materials, billing and revenue. Costs are typically estimated up front, though they may be updated at intervals based on how actual costs compare. Estimates should include all costs arising from or associated with the project — materials and labor, of course, but also asset acquisition, post-completion review and clean-up or decommissioning costs.

While project accountants may use cost accounting methods, they are not required to do so.

**Target costing:** Target costing is a cost accounting technique for companies that operate in a very competitive environment where profit margins are thin, such as construction or consumer goods, setting a hard limit on the amount production costs can rise may be necessary. Target costing defines the maximum a company is willing to pay for production, determined by subtracting the mandated gross margin from the projected product price. If costs start to rise above the target, thereby eating into a company’s profits, production is cut back to bring costs down.

**Lifecycle costing:** Lifecycle costing accounts for all the costs associated with an asset over its life span. For example, a machine has an up-front acquisition cost, but it will also have maintenance, repair and, eventually, disposal costs. Costs may also arise from its operation, such as to offset environmental impact and ensure safety. These lifecycle expenses are estimated to determine the total cost of the machine to the company during its lifetime. This can help managers estimate the real cost of machine used for production.

Cost accounting looks at the cost to produce or deliver goods/service. Life-cycle accounting looks at the cost to acquire and operate a specific piece of equipment. Understanding life-cycle costs can help companies decide if or when to purchase a new piece of equipment.

**Cost volume profit (CVP):** The marginal cost of a product or service is the additional cost of producing one more unit of that product or service. The marginal cost of production falls as production increases because the contribution of fixed costs decreases. A CVP analysis, also known as break-even analysis, uses the marginal production cost to calculate the number of units that must be sold to fully cover the cost of production. CVP analysis is one of many activities performed by cost accountants and breakeven is a key metric for cost accounting.

CVP can be used to estimate the effect of changes in variable and fixed costs and variations in the market price on company profits. For example, suppose a company is forced to discount a product heavily because of a market downturn. CVP can help identify whether the discounting will cause the product to miss its break-even target, and whether reducing production and selling down inventory would help bring it toward break even. Managers can then make an informed decision about whether to continue producing the item at the same volume, cut production to reduce costs or stop producing it until the market improves.


### Formulas for Cost Accounting

Cost accounting includes a variety of concepts and calculations that help a business to determine how well it’s controlling costs and meeting its profit goals. Integrated accounting and financial management software can perform the heavy-lifting, freeing management to focus on the business implications instead.

#### Break-even formula

The break-even point is the point at which a company’s sales exactly cover its total production costs, both fixed and variable. Equating the two determines whether a product is profitable (or not). The break-even formula is:

```
**Break even (in units) =** Total Fixed Costs / Contribution Margin
```

For example, let’s say a bike manufacturer wants to know how many of its newest mountain bikes it needs to sell to break even. Its total fixed costs are $750,000, variable costs per unit are $500 and each bike sells for $600. To calculate the break-even point — in this case, the number of mountain bikes that must be sold — divide $750,000 by $100 ($600 - $500). The result, 7,500, represents how many bikes the company must sell to break even. Multiply 7,500 by $600 (the price per bike) to determine the equivalent break-even point in sales. The answer is $4.5 million.

#### Contribution margin

Contribution margin determines the incremental profit earned for each unit sold after deducting variable costs. It’s the difference between the price charged per unit and the variable costs to produce each unit. In other words, it’s the denominator for the break-even formula.

The formula for contribution is:
```
**Contribution Margin =** Sales Revenue – Variable Costs
```

In the mountain bike example, contribution margin per unit for each new bike is $100 ($600 - $500).

#### Target net income

Breaking even is good, but making a profit is better. Target net income is the amount a business wants to make in profit for a product or service in a given accounting period, after the cost of goods or services (COGS) delivered. The question is how to reach that goal. Using the mountain bike example, let’s say the bike manufacturer sets its profit goal at $2 million. How many bikes does it have to sell?

The formula is:

```
**Unit volume to achieve target net income =** (fixed costs + target net income) / (contribution margin per unit)
```

Let’s plug in the mountain bike numbers: The numerator, 2,750,000 (750,000 + 2,000,000) divided by 100 ($600 - $500) equals 27,500 — the number of bikes that must be sold to reach $2 million. Selling that number of bikes at $600 each would mean $16.5 million in sales — the amount needed in sales to reach $2 million in net income.

#### Gross margin

Businesses use gross margin to benchmark their production costs against their sales revenue. As such, gross margin is the amount of money a company has left after it deducts COGS from net sales. The higher the gross margin, the more a company has earned from a sale after factoring for cost. If gross margin is low, a company may decide to raise prices and/or find ways to cut production costs.

The formula for gross margin is:

```
**Gross margin =** (net sales revenue - COGS) / net sales revenue
```

#### Pre-tax dollars needed for purchase

Although business purchases are usually tax-deductible, they are typically paid from income that has already been taxed and declared for tax purposes in a subsequent accounting period. To pay for these purchases, therefore, a company must earn enough money to cover the cost of the items and the tax it must pay on its income.

The formula to calculate that amount is:

```
**Pre-tax dollars needed for purchase =** cost of item / (1 - tax rate)
```

Imagine a company needs to buy four new office computers for a total of $10,000, and the company’s tax rate on profits is 20%. The cost, $10,000, is divided by 0.80 (1 - 0.20), which comes to $12,500 needed for the purchase.

#### Price variance

Useful for budgeting, price variance is the difference between the standard, or predetermined, cost of a product or service and its actual cost. If the actual cost is less than the standard cost, this is a favorable variance, indicating greater profitability. If actual costs are higher than standard costs, this is an unfavorable variance, indicating loss.

The formula for price variance is:

```
**Price variance =** (actual unit cost - standard unit cost) x number of items purchased
```

#### Efficiency variance

Cost accounting analyzes a number of “input” costs — namely, material, labor and overhead — related to the production of a product or service, and there are efficiency variance formulas for each.

Efficiency variances shed light on operational effectiveness. Many are expressed as costs, but they can also be expressed in quantities like number of hours. They’re all derived from a standard variance formula, which is expressed as the actual units of whatever is being measured minus the expected or budgeted units times the budgeted cost — which is often dollars but might also be hours, for example. For materials, the efficiency variance is referred to as material yield variance; for labor, it’s labor efficiency variance; and for overhead it’s overhead efficiency variance. The formulas are:

```
**Material yield variance =** (actual unit usage - budgeted unit usage) x budgeted cost per unit  
  
**Labor efficiency variance =** (actual per-unit labor hours - budgeted per-unit labor hours) x budgeted hourly labor cost  
  
**Overhead efficiency variance =** (actual per-unit labor hours - budgeted per-unit labor hours) x budgeted overhead rate per unit
```

#### Variable overhead variance

Overhead costs are often the costs companies most want to keep under control. Monitoring variable overhead cost can help, and the formula for doing so is another variation on the classic efficiency variance formula. Variable overhead variance comprises two components: efficiency and overhead spending.

Unlike the labor efficiency variance, the variable overhead efficiency variance considers indirect costs, such as the salaries of office staff and site security.

The formula is:

```
**Variable overhead efficiency variance =** (actual labor hours - budgeted labor hours) x budgeted overhead labor rate

```

Variable overhead spending variance compares the actual overhead cost of production with the budgeted or expected cost. The formula is:

```
**Variable overhead spending variance =** (actual overhead labor rate - budgeted overhead labor rate) x actual labor hours
```

#### Ending inventory

Generally speaking, ending inventory is the value of finished goods that remain for sale at the end of an accounting period. In production, inventory also includes raw materials, labor and overhead. Ending inventory appears as a current asset on a company’s [balance sheet](https://www.netsuite.com/portal/resource/articles/accounting/balance-sheet.shtml). The basic formula is:

```
**Ending inventory =** beginning inventory + net purchases - cost of goods sold
```

Net purchases account for goods purchased for a product or service less anything returned, reduced in price due to a problem or discounted, perhaps due to a promotion. The formula is:

```
**Net purchases =** purchases - returns - allowances - discounts
```

While the concept of ending inventory is straightforward, how much the goods are determined to be worth [depends on the valuation method used](https://www.netsuite.com/portal/resource/articles/accounting-software/fifo-lifo-average-comparing-the-accounting-software-inventory-costing-methods.shtml).

The four main methods are:

**First in, first out (FIFO)** assumes the oldest inventory sold first. During times of inflation, this method calculates a higher value for ending inventory, lower cost of goods sold and a higher gross profit.

**Last in, first out (LIFO)** assumes the newest inventory sold first. During times of inflation, this method can result in lower net income values and a decreased ending inventory value.

**Weighted cost average** averages the cost of new inventory purchases with the cost of existing inventory. This method is a contender for companies that sell similarly priced products.

**Specific identification** tracks the cost of each piece of inventory and the actual price of each item sold. Companies that sell high-cost items, such as cars and jewelry, generally use this method.


### Examples of Cost Accounting

Here’s an example of cost accounting for a typical small manufacturing company we’ll call “Bellmore Gizmos.” The company produces a variety of widgets, but they all have roughly the same costs of production. Bellmore Gizmos uses standard cost accounting, which means overhead costs are allocated across the entire production.

This is the simplest form of cost accounting. Here is what its basic actual cost card might look like:

| **Cost category** | **$000s** |
| --- | ---: |
| Direct materials | 600 |
| Direct labor | 1,200 |
| Indirect materials | 100 |
| Overhead: ||
	| Sales & marketing | 200 |
	| Non-production staff costs | 800 |
	| Payroll taxes & persion contributions | 400 |
	| Utilities | 200 |
	| Permises costs | 600 |
	| Depreciation | 150 |
	| Interest cost | 50 |
| Total overhead | 2,400 |
| **TOTAL COST** | 4,300 |

But that’s only part of the picture. Bellmore Gizmos also wants to compare actual costs to budgeted costs, to determine the accuracy (or not) of its estimates. Gaps between the two are known as variances, and they’re either favorable, meaning profitable, or unfavorable, meaning loss-making.

| **Cost category** | **Budget ($000s)** | **Actual ($000s)** | ** Varlance (%)** |
| --- | ---: | ---: | ---: |
| Direct materials | 700 | 600 | 14.3% | 
| Direct labor | 1,200 | 1,200 | 0% |
| Indirect materials | 50 | 100 | -100% |
| Overhead: |||
	| Sales & marketing | 250 | 200 | 20% |
	| Non-production staff costs | 700 | 800 | -14.3% |
	| Payroll taxes & persion contributions | 350 | 400 | -14.3 |
	| Utilities | 200 | 200 | 0% |
	| Permises costs | 600 | 600 | 0% |
	| Depreciation | 150 | 150 | 0% |
	| Interest cost | 50 | 50 | 0% |
| Total overhead | 2,300 | 2,400 | -4.53% |
| **TOTAL COST** | 4,250 | 4,300 | -1.18% |

It appears as if Bellmore Gizmos has saved quite a bit on its direct materials, though we don’t know whether that’s due to better supplier terms or lower production. This saving is offset by higher overhead.

The purpose of cost accounting is to enable managers to budget, identify where efficiency savings can be made and improve profits. To gain a full picture of how these changes in the company’s costs affect its bottom line, we need production and sales data. Here’s that data for the same period as the cost figures above:

| **Product** | **Unit price ($)** | **No. produced** | **No. sold** | **Sales revenue (%000s)** | ** Inventory ($000s)$** |
| :--- | ---: | ---: | ---: | ---: | ---: |
| Gizmo A | 5.00 | 300.000 | 250,000 | 1,250 | 250 |
| Gizmo A | 7.50 | 300.000 | 290,000 | 2,175 | 75 |
| Gizmo A | 9.00 | 150.000 | 150,000 | 1,350 | 0 |
| Gizmo A | 13.00 | 100.000 | 80,000 | 1,300 | 260 |
| **TOTAL** ||**850.000** | **770,000** | **6,075** | **585** |

A simple profit formula (Profit = sales - fixed and variable costs) shows Bellmore Gizmos is profitable. Taking the numbers from these charts shows: Profit = $6,075,000 - $1,900,000 (actual variable cost) - $2,400,000 (actual fixed cost), or $1,775,000.

However, because a standard costing approach results in some product lines contributing more than others, a company might want to do break-even and profit analysis by product line using cost value profit (CVP) analysis. They might also consider switching to activity-based costing (ABC) to match costs to products more accurately.


### Cost Accounting Principles to Know

Cost accounting principles dictate how expenses and revenue are recorded. Here are two to understand.

#### Matching principle

In accounting, the “matching principle” requires a company to report expenses in the same accounting period as related revenues for a product or service. This provides a more accurate picture of a company’s operations on its income statement. If 500 mountain bikes are sold in April, their cost to manufacturer is recognized in April, too.

#### Principle of conservatism

Accounting is not always precise, and sometimes accountants need to make decisions about how best to show financial outcomes. The principle of conservatism per [Generally Accepted Accounting Principles (GAAP)](https://www.netsuite.com/portal/resource/articles/accounting/general-accepted-accounting-principles-gaap.shtml) says those decisions should present the most cautious, or pessimistic, view of the company’s finances.


### History of Cost Accounting

Modern cost accounting is thought to have started during the Industrial Revolution, which began in Great Britain in the late 1700s and spread to the United States around 1820. Businesses have always needed to track and manage costs, but prior to the advent of mass production, businesses tended to be small and costs were principally direct variable costs — mostly labor and materials. Then, cost accounting was largely a matter of managing cash flow.

During the Industrial Revolution, much larger and more complex businesses emerged, notably to produce industrial goods such as steel for railroads. These businesses had fixed costs that could not easily be associated with the products they produced — what we now call overhead. Cost accounting developed as a means of enabling organizations to allocate overhead to product production and thus help them make better decisions regarding pricing, investments and product development.

In the past few decades, developments in management theory and business practice have led to the growth of new kinds of cost accounting. These include activity-based accounting, lean accounting and environmental cost accounting.


### Minimize Cost Accounting Efforts With Accounting Software

Cost accounting is often detailed and complex. Automating the process with [modern accounting software](https://www.netsuite.com/portal/products/erp/financial-management/finance-accounting.shtml) removes much of the time and expense traditionally involved with manual cost accounting, while providing internal management with the information to inform budgeting decisions, product/service pricing and business strategy.

The most capable accounting software lets business managers set up cost categories and profit centers as they see fit. Accounting software automatically tracks expenses against budget and advanced versions flag variances. It can also calculate break-even points, gross margin and other key metrics that help managers identify potential savings, potential areas of loss, and potential areas of opportunity.

A long way since the Industrial Revolution. A form of management accounting, cost accounting evaluates a company’s total costs to produce its products or services and is meant to be used by internal stakeholders only. Costs, both variable and fixed, include materials, labor and overhead. Cost accounting helps companies identify areas where they may be better able to control their costs, as well as set or adjust pricing to maintain profitability.


** Original **: [Cost Accounting Defined: What It Is & Why It Matters](https://www.netsuite.com/portal/resource/articles/accounting/cost-accounting.shtml)
** Author **: [David Luther](https://www.netsuite.com/portal/resource/authors/david-luther.shtml)