# Overview
침입 방지 시스템(IPS)은 네트워크 트래픽에서 잠재적인 위협을 모니터링하고 보안 팀에 경고, 위험한 연결 종료, 악성 콘텐츠 제거 또는 기타 보안 장치를 트리거하는 등 자동으로 차단 조치를 취합니다. 

IPS 솔루션은 위협을 탐지하여 보안 팀에 보고하는 [침입 탐지 시스템(IDS)](https://www.ibm.com/kr-ko/topics/intrusion-detection-system)에서 발전했습니다. IPS는 IDS와 동일한 위협 탐지 및 보고 기능에 자동화된 위협 방지 기능을 갖추고 있기 때문에 "침입 탐지 및 방지 시스템"(IDPS)이라고도 합니다.

IPS는 악의적인 트래픽을 직접 차단할 수 있기 때문에 보안 팀과 [보안 운영 센터](https://www.ibm.com/kr-ko/topics/security-operations-center)(SOC)의 워크로드를 줄여 더 복잡한 위협에 집중할 수 있도록 해 줍니다.IPS는 합법적인 사용자의 무단 행동을 차단하여 [네트워크 보안](https://www.ibm.com/kr-ko/topics/network-security) 정책을 시행하는 데 도움이 되며 규정 준수 노력을 지원할 수 있습니다. 예를 들어, IPS는 침입 탐지 조치에 대한 결제 카드 산업 데이터 보안 표준(PCI-DSS) 요구 사항을 충족합니다

## IPS threat detection methods
IPS는 세 가지 기본 위협 탐지 방법을 단독으로 또는 조합하여 사용하여 트래픽을 분석합니다.

### Signature-based detection 
시그니처 기반 탐지 방법은 네트워크 패킷을 분석하여 공격 시그니처(특정 위협과 관련된 고유한 특성 또는 행동)를 찾아내는 방법입니다. 특정 [멀웨어](https://www.ibm.com/kr-ko/topics/malware) 변종에 나타나는 일련의 코드는 공격 시그니처의 예입니다.

시그니처 기반 IPS는 네트워크 패킷을 비교하는 공격 시그니처 데이터베이스를 유지 관리합니다. 패킷이 시그니처 중 하나와 일치하는 것을 트리거하면 IPS가 조치를 취합니다. 새로운 [사이버 공격](https://www.ibm.com/kr-ko/topics/cyber-attack)이 출현하고 기존 공격이 진화함에 따라 시그니처 데이터베이스는 새로운 [위협 인텔리전스](https://www.ibm.com/kr-ko/topics/threat-intelligence)로 정기적으로 업데이트되어야 합니다. 그러나 아직 시그니처가 분석되지 않은 새로운 공격은 시그니처 기반 IPS를 회피할 수 있습니다.

### Anomaly-based detection
이상 징후 기반 탐지 방법은 인공 지능과 [머신 러닝](https://www.ibm.com/kr-ko/topics/machine-learning)을 사용하여 정상적인 네트워크 활동의 기준 모델을 생성하고 지속적으로 개선합니다. IPS는 진행 중인 네트워크 활동을 모델과 비교하여 평소보다 더 많은 대역폭을 사용하는 프로세스나 일반적으로 닫혀 있는 포트를 여는 장치와 같은 편차를 발견하면 조치를 취합니다.

이상 행위 기반 IPS는 모든 비정상적인 행동에 대응하기 때문에 시그니처 기반 탐지를 회피할 수 있는 새로운 사이버 공격을 차단할 수 있는 경우가 많습니다. 소프트웨어 개발자가 [소프트웨어 취약점](https://www.ibm.com/kr-ko/topics/vulnerability-management)을 인지하거나 [패치](https://www.ibm.com/kr-ko/topics/patch-management)를 적용할 시간을 갖기 전에 이를 악용하는 공격인 제로데이 익스플로잇을 포착할 수도 있습니다.

그러나 이상 징후 기반 IPS는 오탐이 발생할 가능성이 더 높습니다. 권한이 부여된 사용자가 민감한 네트워크 리소스에 처음으로 액세스하는 것과 같은 정상 활동도 이상 징후 기반 IPS를 트리거할 수 있습니다. 결과적으로 인증된 사용자는 네트워크에서 부팅되거나 IP 주소가 차단될 수 있습니다. 

### Policy-based detection
정책 기반 탐지 방법은 보안 팀에서 설정한 보안 정책을 기반으로 합니다. 정책 기반 IPS는 보안 정책을 위반하는 행위를 탐지할 때마다 해당 시도를 차단합니다.

예를 들어 SOC는 호스트에 액세스할 수 있는 사용자 및 장치를 지정하는 액세스 제어 정책을 설정할 수 있습니다. 승인되지 않은 사용자가 호스트에 연결을 시도하면 정책 기반 IPS가 이를 중지합니다.

정책 기반 IPS는 맞춤화 기능을 제공하지만 상당한 사전 투자가 필요할 수 있습니다. 보안 팀은 네트워크 전체에서 허용되는 것과 허용되지 않는 것을 간략하게 설명하는 포괄적인 정책 집합을 만들어야 합니다. 

### Less common threat detection methods

대부분의 IPS는 위에서 설명한 위협 탐지 방법을 사용하지만 일부는 덜 일반적인 기술을 사용합니다.

**평판 기반 탐지**는 악의적이거나 의심스러운 활동과 관련된 IP 주소 및 도메인의 트래픽을 플래그 지정하고 차단합니다. **스테이트풀 프로토콜 분석**은 프로토콜 동작에 중점을 둡니다. 예를 들어, 단일 IP 주소가 단기간에 여러 개의 TCP 연결을 동시에 요청하는 것을 감지하여 [분산 서비스 거부](https://www.ibm.com/kr-ko/topics/ddos)(DDoS) 공격을 식별할 수 있습니다.

## IPS threat prevention methods
IPS는 위협을 탐지하면 이벤트를 로그하고 [보안 정보 및 이벤트 관리(SIEM)](https://www.ibm.com/kr-ko/topics/siem) 툴을 통해 SOC에 보고합니다(아래 "IPS 및 기타 보안 솔루션" 참조).

하지만 IPS는 여기서 멈추지 않습니다. 다음과 같은 기술을 사용하여 위협에 대해 자동으로 조치를 취합니다.

##### 악성 트래픽 차단

IPS는 사용자의 세션을 종료하거나, 특정 IP 주소를 차단하거나, 대상에 대한 모든 트래픽을 차단할 수도 있습니다. 일부 IPS는 트래픽을 허니팟으로 리디렉션하여 해커가 실제로는 SOC가 감시하고 있는데도 성공했다고 착각하게 만드는 미끼 자산으로 트래픽을 리디렉션할 수 있습니다. 

##### 악성 콘텐츠 제거

IPS는 트래픽이 계속되도록 허용하지만 스트림에서 악성 패킷을 삭제하거나 이메일에서 악성 첨부 파일을 제거하는 등 위험한 부분을 제거할 수 있습니다.

##### 다른 보안 장치 트리거

IPS는 방화벽 규칙을 업데이트하여 위협을 차단하거나 라우터 설정을 변경하여 해커가 공격 대상에 도달하지 못하도록 하는 등 다른 보안 장치에 조치를 취하도록 유도할 수 있습니다.

보안 정책 적용

일부 IPS는 공격자와 권한이 없는 사용자가 회사 보안 정책을 위반하는 모든 행위를 방지할 수 있습니다. 예를 들어, 사용자가 데이터베이스 밖으로 중요한 정보를 전송하려고 하면 IPS는 이를 차단합니다.
## Types of intrusion prevention systems

IPS 솔루션은 엔드포인트에 설치되는 소프트웨어 애플리케이션, 네트워크에 연결된 전용 하드웨어 디바이스 또는 클라우드 서비스로 제공될 수 있습니다. IPS는 악의적인 활동을 실시간으로 차단할 수 있어야 하기 때문에 네트워크에 항상 "인라인"으로 배치되므로 트래픽이 목적지에 도달하기 전에 IPS를 직접 통과합니다.

IPS는 네트워크의 위치와 모니터링하는 활동의 종류에 따라 분류됩니다. 많은 조직이 네트워크에서 여러 유형의 IPS를 사용합니다.
### Network-based intrusion prevention systems (NIPS)

A network-based intrusion prevention system (NIPS) monitors inbound and outbound traffic to devices across the network, inspecting individual packets for suspicious activity. NIPS monitors are placed at strategic points in the network. They often sit immediately behind firewalls at the network perimeter so they can stop malicious traffic breaking through. NIPS's may also be placed inside the network to monitor traffic to and from key assets, like critical [data centers](https://www.ibm.com/topics/data-centers) or devices. 
네트워크 기반 침입 방지 시스템(NIPS)은 네트워크를 통해 디바이스로 향하는 인바운드 및 아웃바운드 트래픽을 모니터링하여 개별 패킷에 의심스러운 활동이 있는지 검사합니다. NIPS는 네트워크의 전략적 지점에 배치됩니다. 이들은 침입하는 악성 트래픽을 차단할 수 있도록 네트워크 경계의 방화벽 바로 다음에 위치하는 경우가 많습니다. 중요한 [데이터 센터](https://www.ibm.com/kr-ko/topics/data-centers)나 디바이스와 같은 주요 자산과 주고받는 트래픽을 모니터링하기 위해 네트워크 내부에 NIPS를 배치할 수도 있습니다.

### Host-based intrusion prevention systems (HIPS)

A host-based intrusion prevention system (HIPS) is installed on a specific endpoint, like a laptop or server, and monitors only traffic to and from that device. HIPS are usually used in conjunction with NIPS to add extra security to vital assets. HIPS can also block malicious activity from a compromised network node, like [ransomware](https://www.ibm.com/topics/ransomware) spreading from an infected device. 
호스트 기반 침입 방지 시스템(HIPS)은 노트북이나 서버와 같은 특정 엔드포인트에 설치되며 해당 장치와 주고받는 트래픽만 모니터링합니다. HIPS는 일반적으로 중요한 자산에 추가 보안을 추가하기 위해 NIPS와 함께 사용됩니다. 또한 HIPS는 감염된 디바이스에서 확산되는 [랜섬웨어](https://www.ibm.com/kr-ko/topics/ransomware)와 같이 손상된 네트워크 노드에서 발생하는 악성 활동을 차단할 수 있습니다.
### Network behavior analysis (NBA)

Network behavior analysis (NBA) solutions monitor network traffic flows. NBAs may inspect packets like other IPSs but many NBAs focus on higher-level details of communication sessions, such as source and destination IP addresses, ports used and the number of packets transmitted.
네트워크 행동 분석(NBA) 솔루션은 네트워크 트래픽 흐름을 모니터링합니다. NBA는 다른 IPS처럼 패킷을 검사할 수도 있지만, 많은 NBA는 소스 및 대상 IP 주소, 사용된 포트, 전송된 패킷 수 등 통신 세션에 대한 상위 수준의 세부 정보에 중점을 둡니다.

NBAs use anomaly-based detection methods, flagging and blocking any flows that deviate from the norm, like a  DDoS attack on traffic or a malware-infected device communicating with an unknown command and control server.
NBA는 이상 징후 기반 탐지 방법을 사용하여 DDoS 공격 트래픽이나 맬웨어에 감염된 디바이스가 알 수 없는 명령 및 제어 서버와 통신하는 등 정상에서 벗어나는 모든 흐름에 플래그를 지정하고 차단합니다.

### Wireless intrusion prevention systems (WIPS)

A wireless intrusion prevention system (WIPS) monitors wireless network protocols for suspicious activity, like unauthorized users and devices accessing the company's wifi. If a WIPS detects an unknown entity on a wireless network, it can terminate the connection. A WIPS can also help detect misconfigured or unsecured devices on a wifi network and intercept man-in-the-middle attacks, where a hacker secretly spies on users' communications.
무선 침입 방지 시스템 (WIPS)은 무선 네트워크 프로토콜을 모니터링하여 회사의 Wi-Fi에 액세스하는 승인되지 않은 사용자 및 장치와 같은 의심스러운 활동이 있는지 확인합니다. WIPS가 무선 네트워크에서 알 수 없는 개체를 감지하면 연결을 종료할 수 있습니다. 또한 WIPS는 Wi-Fi 네트워크에서 잘못 구성되거나 보안되지 않은 장치를 탐지하고 해커가 사용자의 통신을 몰래 감시하는 중간자 공격을 차단하는 데 도움이 될 수 있습니다.

## IPS and other security solutions

IPS는 독립형 툴로 사용할 수 있지만, 전체적인 사이버 보안 시스템의 일부로 다른 보안 솔루션과 긴밀하게 통합되도록 설계되었습니다.

### IPS and SIEM (security information and event management)

IPS 알림은 종종 조직의 [SIEM](https://www.ibm.com/kr-ko/topics/siem)으로 전달되며, 여기서 다른 보안 툴의 알림 및 정보와 결합하여 중앙 집중식 단일 대시보드에서 확인할 수 있습니다. 보안 팀은 IPS를 SIEM과 통합하면 추가적인 위협 인텔리전스로 IPS 알림을 강화하고, 잘못된 경보를 필터링하고, IPS 활동을 추적하여 위협이 성공적으로 차단되었는지 확인할 수 있습니다. 또한 많은 조직에서 두 가지 이상의 유형을 사용하기 때문에 SIEMS는 SOC가 다양한 종류의 IPS에서 데이터를 조정하는 데 도움이 될 수 있습니다. 

### IPS and IDS (intrusion detection system)

앞서 언급했듯이 IPS는 IDS에서 진화했으며 동일한 기능을 다수 가지고 있습니다. 일부 조직에서는 별도의 IPS 및 IDS 솔루션을 사용하기도 하지만, 대부분의 보안 팀은 강력한 탐지, 로그, 보고 및 자동 위협 방지 기능을 제공하는 단일 통합 솔루션을 배포합니다. 많은 IPS를 통해 보안 팀은 예방 기능을 차단하여 조직이 원하는 경우 순수한 IDS 역할을 할 수 있습니다. 

# IPS and firewalls
IPS는 방화벽 다음의 두 번째 방어선 역할을 합니다. 방화벽은 경계에서 악의적인 트래픽을 차단하고 IPS는 방화벽을 뚫고 네트워크에 침입하는 모든 것을 차단합니다. 특히 차세대 방화벽을 비롯한 일부 방화벽에는 IPS 기능이 내장되어 있습니다.