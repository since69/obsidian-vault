# Container

### Dotnet

- Create *Dockerfile*
- Create image
```bash
docker build . -t Sfd.WebApp:0.1
```
- Run
```bash
docker run -p 5000:80 Sfd.WebApp:0.1
```