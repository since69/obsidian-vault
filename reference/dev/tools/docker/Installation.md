# Overview

Docker desktop includes are following:

- [Docker Engine](https://docs.docker.com/engine/)
- Docker CLI client
- Docker Scout
- Docker Build
- Docker Extension
- Docker Compose
- Docker Content Trust
- [Kubernetes](https://github.com/kubernetes/kubernetes/)
- Credential Helper

# Install Docker Desktop

### Windows


### Linux

##### Ubuntu

1. Set up Docker's *apt* repository.
```Bash
# show distribution version
lsb_release -a

# add docker's official gpg key:
sudo apt-get update
sudo apt-get install ca-certificates curl
sudo install -m 0755 -d /etc/apt/keyrings
sudo curl -fsSL https://download.docker.com/linux/ubuntu/gpg -o /etc/apt/keyrings/docker.asc
sudo chmod a+r /etc/apt/keyrings/docker.asc

# add the repository to Apt sources:
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.asc] https://download.docker.com/linux/ubuntu \
  $(. /etc/os-release && echo "$VERSION_CODENAME") stable" | \
  sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update
```
2. Install the Docker packages.
```bash
sudo apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
```
3. Verify with *hello-world image.
```bash
sudo docker run hello-world
```

### Common / Linux
- Add user to docker group
```bash
# add user to docker user group
sudo gpawwd -a "${USER}" docker
```