
### Location

- '/etc/gitconfig'
- ~/.gitconfig
- ~/.config/git/config

### 사용자 정보 (Global)

- 설정
```Bash
git config --global user.name "Your name"
git config --global user.email "YOur email"
```
- 확인
```Bash
git config --list
```
