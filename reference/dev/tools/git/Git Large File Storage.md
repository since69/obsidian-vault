
# An open source Git extension for versioning large files

Git Large File Storage (LFS) replaces large files such as audio samples, videos, datasets, and graphics with text pointers inside Git, while storing the file contents on a remote server like GitHub.com or GitHub Enterprise.

![[graphic.gif]]


### Usage

##### 1. Install
Download and install the [Git command line extension](https://github.com/git-lfs/git-lfs/releases/download/v3.6.0/git-lfs-windows-v3.6.0.exe). Once downloaded and installed, set up Git LFS for your user account by running:

```PowerShell
git lfs install
```

 ##### 2. Clone

```PowerShell
git lfs fetch origin main
```

 ##### 3.  Add extension

```PowerShell
git lfs track "*.pptx"
```

