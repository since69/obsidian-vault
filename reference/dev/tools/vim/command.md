# Pane

- `:vsplit` - create a vertical split
- `:split` - create a horizontal split
- `:e test.cpp` - open test.cpp
- `:term` - replace the content of the second horizontal split by a terminal
# Copy, cut and paste


Here is how to cut-and-paste or copy-and-paste text using a visual selection in Vim. See [Cut/copy and paste using visual selection](http://vim.fandom.com/wiki/Cut/copy_and_paste_using_visual_selection "Cut/copy and paste using visual selection") for the main article.

**Cut and paste:**

1. Position the cursor where you want to begin cutting.
2. Press `v` to select characters, or uppercase `V` to select whole lines, or `Ctrl-v` to select rectangular blocks (use `Ctrl-q` if `Ctrl-v` is mapped to paste).
3. Move the cursor to the end of what you want to cut.
4. Press `d` to cut (or `y` to copy).
5. Move to where you would like to paste.
6. Press `P` to paste before the cursor, or `p` to paste after.

**Copy and paste** is performed with the same steps except for step 4 where you would press `y` instead of `d`:

- `d` stands for _delete_ in Vim, which in other editors is usually called _cut_
- `y` stands for _yank_ in Vim, which in other editors is usually called _copy_

## Copying and cutting in normal mode

In normal mode, one can copy (yank) with `y{motion}`, where `{motion}` is a Vim motion. For example, `yw` copies to the beginning of the next word. Other helpful yanking commands include:

- `yy` or `Y` – yank the current line, including the newline character at the end of the line
- `y$` – yank to the end of the current line (but don't yank the newline character); note that many people like to remap `Y` to `y$` in line with `C` and `D`
- `yiw` – yank the current word (excluding surrounding whitespace)
- `yaw` – yank the current word (including leading or trailing whitespace)
- `ytx` – yank from the current cursor position up to and before the character (til `x`)
- `yfx` – yank from the current cursor position up to and including the character (find `x`)

Cutting can be done using `d{motion}`, including:

- `dd` - cut the current line, including the newline character at the end of the line

To copy into a register, one can use `"{register}` immediately before one of the above commands to copy into the register `{register}`. See [pasting registers](http://vim.fandom.com/wiki/Pasting_registers "Pasting registers") for more information on register syntax.

## Pasting in normal mode

In normal mode, one can use `p` to paste after the cursor, or `P` to paste before the cursor.

The variants `gp` and `gP` move the cursor after the pasted text, instead of leaving the cursor stationary.

To select a register from which to paste, one can use `"{register}p` to paste from the register `{register}`. See [pasting registers](http://vim.fandom.com/wiki/Pasting_registers "Pasting registers").

## Pasting in insert mode

The contents of a register can be pasted while in insert mode: type Ctrl-r then a character that identifies the register. For example, Ctrl-r then `"` pastes from the default register, and Ctrl-r then `0` pastes from register zero which holds the text that was most recently yanked (copied). See [pasting registers](http://vim.fandom.com/wiki/Pasting_registers "Pasting registers").

## Copying and cutting in command-line mode

Command-line mode occurs after typing `:` to enter a command. By default, while in the command line, typing Ctrl-f opens the command-line window where commands can be edited using normal mode. For example, part of one command can be copied then pasted into another command. See [using command-line history](http://vim.fandom.com/wiki/Using_command-line_history "Using command-line history").

## Pasting in command-line mode

There are two approaches to pasting in command-line mode. The first is to open the command-line window with Ctrl-f, then use normal-mode commands to paste. See the [previous section](http://vim.fandom.com/wiki/Copy,_cut_and_paste#Copying_and_cutting_in_command-line_mode).

The second approach is to type Ctrl-r then a character to paste the contents of the register identified by the character. See [Pasting in insert mode](http://vim.fandom.com/wiki/Copy,_cut_and_paste#Pasting_in_insert_mode) above.

## Copy, cut, and paste from the system clipboard

_Main article: [Accessing the system clipboard](http://vim.fandom.com/wiki/Accessing_the_system_clipboard "Accessing the system clipboard")_

Unlike most text editors, Vim distinguishes between its own registers and the system clipboard. By default, Vim copies to, cuts to, and pastes from its own default register, called the _unnamed register_ (`""`, also called [quotequote](http://vimdoc.sourceforge.net/htmldoc/change.html#quote_quote)) instead of the system clipboard.

Assuming Vim was compiled with clipboard access, it is possible to access the `"+` or `"*` registers, which can modify the system clipboard. In this case, one can copy with e.g. `"+y` in visual mode, or `"+y{motion}` in normal mode, and paste with e.g. `"+p`.

If your installation of Vim was not compiled with clipboard support, you must either install a package that has clipboard support, or use an external command such as xclip as an intermediary. See [Accessing the system clipboard](http://vim.fandom.com/wiki/Accessing_the_system_clipboard "Accessing the system clipboard") for detailed information.

## Multiple copying

_Main article: [Pasting registers](http://vim.fandom.com/wiki/Pasting_registers "Pasting registers")._

Deleted or copied text is placed in the unnamed register. If wanted, a register can be specified so the text is also copied to the named register. A register is a location in Vim's memory identified with a single letter. A double quote character is used to specify that the next letter typed is the name of a register.

For example, you could select the text `hello` then type `"ay` to copy "hello" to the `a` register. Then you could select the text `world` and type `"by` to copy "world" to the `b` register. After moving the cursor to another location, the text could be pasted: type `"ap` to paste "hello" or `"bp` to paste "world". These commands paste the text after the cursor. Alternatively, type `"aP` or `"bP` to paste before the cursor.

### Windows clipboard

When using Vim under Windows, the clipboard can be accessed with the following:

- In step 4, press Shift+Delete to cut or Ctrl+Insert to copy.
- In step 6, press Shift+Insert to paste.

### Different instances

How does one copy and paste between two instances of Vim on different Linux consoles?

After copying text, open a new buffer for a new file:

:e ~/dummy

- Paste the text to the new buffer.
- Write the new buffer `:w`.
- Switch to the previous buffer `:bp` to release *.swp.
- Now switch to the other console.
- Put the cursor at the desired place.
- Read the dummy file `:r ~/dummy`

## Increasing the buffer size

By default, only the first 50 lines in a register are saved, and a register is not saved if it contains more than 10 kilobytes. [:help 'viminfo'](http://vimdoc.sourceforge.net/cgi-bin/help?tag=%27viminfo%27)

In the example below, the first line displays the current settings, while the second line sets:

- `'100` [Marks](http://vim.fandom.com/wiki/Using_marks "Using marks") will be remembered for the last 100 edited files.
- `<100` Limits the number of lines saved for each register to 100 lines; if a register contains more than 100 lines, only the first 100 lines are saved.
- `s20` Limits the maximum size of each item to 20 kilobytes; if a register contains more than 20 kilobytes, the register is not saved.
- `h` Disables [search highlighting](http://vim.fandom.com/wiki/Highlight_all_search_pattern_matches "Highlight all search pattern matches") when Vim starts.

:set viminfo?
:set viminfo='100,<100,s20,h

## See also

- [Quick yank and paste](http://vim.fandom.com/wiki/VimTip356 "VimTip356")
- [Cut/copy and paste using visual selection](http://vim.fandom.com/wiki/Cut/copy_and_paste_using_visual_selection "Cut/copy and paste using visual selection")
- [In line copy and paste to system clipboard](http://vim.fandom.com/wiki/In_line_copy_and_paste_to_system_clipboard "In line copy and paste to system clipboard")
- [Accessing the system clipboard](http://vim.fandom.com/wiki/Accessing_the_system_clipboard "Accessing the system clipboard")