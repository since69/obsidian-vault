
# Format

### Formats
- Allman
```cpp
while (x == y)
{
	func1();
	func2();
}
```
- Kernighan & RItche
```cpp
while (x == y) {
	func1();
	func2();
}
```
- GNU
```cpp
while (x == y)
	{
		func1();
		func2();
	}
```
- Whitesmiths
```cpp
while (x == y)
	{
	func1();
	func2();
	}
```
- Horstmann
```cpp
while (x == y)
{
	func1();
	func2();
}
```
- Haskell style
```cpp
while (x == y)
	{ func1()
	; func2()
	;
	}
```
- Ratliff style
```cpp
while (x == y) {
	func1();
	func2();
	}
```
- Lisp style
```cpp
while (x == y)
	{ func1();
	  func2(); }
```

### Assign to NVIM

1. Generate from Template
``` console
~/.local/share/nvim/mason/bin/clang-format --style GNU --dump-config > .clang-format
```
2. Open NeoVim
3. **Space**(Ledder key) + **f** and **m**

