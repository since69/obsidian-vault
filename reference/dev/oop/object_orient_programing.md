
### Base

- Abstract
- Encapsulation
- Polymorphism
- Inheritance

### Principles

- 프로그램에서 달라지는 부분을 찾아내고, 달라지지 않는 부분과 분리한다.
- 구현보다는 인터페이스에 맞춰서 작성한다.
- 상속보다는 구성을 활용한다.

### Object Oriented Patterns

- #### Strategy Pattern

> 알고리즘군을 정의하고 캡슐화해서 각각의 알고리즘군을 수정해서 사용할 수 있도록 해줌.
> 전략 패턴을 사용하면 클라이언트로부터 알고리즘을 분리해서 독립적으로 변경 가능.