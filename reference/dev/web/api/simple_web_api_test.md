1. Create html file.
2. Edit following:

```Html
GET http://localhost:8000/games

###
GET http://localhost:8000/games/1

###
POST http://localhost:8000/games
Content-Type: application/json

{
	"name": "Minecraft",
	"gener": "Kids and Family",
	"price": 19.00,
	"releasedDate": "2011-11-18"
}

###
PUT http://localhost:8000/games/4
Content-Type: application/json

{
	"name": "Minecraft 2",
	"gener": "Kids and Family",
	"price": 22.00,
	"releasedDate": "2012-11-20"
}

###
DELETE http://localhost:8000/games/2
```

3. Mouse R-Click and "Send Request" Select. 