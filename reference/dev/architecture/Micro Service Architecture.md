
# Overview

An architectural style that structures an application as a collection of independently deployable services that are modeled around a business domain and are usually owned by a samll team.

# When to use Microservices ?
- It's perfectly fine to start with a monolith, then move to microservices
- Start looking at microservices when:
	- The code base size is more then what a small team can maintain
	- Teams can't  move fast any more
	- Builds become too slow due to large code base
	- Time to market is compromised due to infrequent deployment and long verification times
	- It's all about team autonomy!

# Pros & Cons

## Monolith 

##### PROS
- Convenient for new project
- Tools mostly focused on them
- Great code reuse
- Easier to run locally
- Easier to debug and troubleshoot
- One thing to build
- One thing to deploye
- One thing to text end to end
- One thing to scale
##### CONS
- Easily gets too complex to understand
- Merging code can be challenging
- Slows down IDEs
- Long build times
- Slow and infrequent deployment
- Long testing and stabilization periods
- **Rolling back is all or nothing**
- No isolation between modules
- Can be hard to scale
- Hard to adopt new tech
## Microservices 

##### PROS
- Small, easier to understand code base
- Quicker to build
- Independent, faster deployment and rollbacks
- Independent scalable
- Much better isolation from failures
- Designed for continuos delivery
- Easier to adopt new varied tech
- Grants autonomy to teams and lets them work in parallel
- Can't use transaction across services
##### CONS
- Not easy to find the right set of services
- Add the complexity of distributed systems
- Shared code moves to seperate libraries
- No good tooling for distributed apps
- Releasing features across services is hard
- Hard to troubleshoot issues across services
- Raises the required skillset for teams
