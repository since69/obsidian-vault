
# Overview

**마이크로 서비스 아키텍처(Micro Service Architecture)** 형태의  서비스는 서비스의 복잡도를 줄일 수 있고, 변경에 따른 영향을 최소화하면서 개발과 배포가 가능한 장점을 가진 반면 마이크로 서비스가 증가할 경우, 이 서비스들의 엔드포인트 관리의 어려움이 생기고, 또 공통 서비스(ex 인증/인가, 로깅 등)들의 중복 개발 문제점 발생.

이러한 문제점 해결을 위해 해결책으로 **API Gateway** 등장. API Gateway는 클라이언트와 개별 서비스들 사이에 위치. (아래 그림 참조)
![[apigateway.jpg]]

클라이언트는 각 서비스의 엔드포인트 대신 **API Gateway로 요청을 보내며, 요청을 받은 API Gateway는 설정에 따라 각 엔드포인트로 클라이언트를 대신하여 요청하고, 응답을 받으면 다시 클라이언트에게 전달하는 프록시(proxy) 역할**을 하게 됨.

이처럼 API Gateway는 클라이언트의 요청을 라우팅 하는 것 외에도 **단일 진입점의 이점**으로 인해 추가로 구현 시 다수의 유용한 기능들을 갖게 됨.


# Main Feature of API Gateway

### Generate token for Authentication / Authorization 

모든 서비스에 공통으로 구현되어야 할 인증([[Authentication]])/인가([[Authorization]])의 경우 개별 서비스마다 구현은 매우 비효율임으로 API Gateway에서 이 처리를 대신 함.

![](https://blog.kakaocdn.net/dn/RPkXR/btrREWSgyr7/v9rNCKPxPUSasM7oNF0fRK/img.jpg)
*인증 서버*

API 사용을 위한 토큰 발급 또한 마찬가지 이유로 API Gateway에서 처리하며, 실제 토큰 발급 기능은 인증을 위한 서비스(= 인증 서버)를 하나 두고 거기에서 처리.

### 공통로직 처리

인증/인가 외에 다수의 서비스에서 공통으로 처리해야 할 기능을 API Gateway에서 처리하여 개별 서비스마다 구현하는 것은 번거로움을 피하여, 유지보수 측면에서의 어려움을 피.

![](https://blog.kakaocdn.net/dn/IrGCf/btrRD0gfDMn/s5AKsabDLVNZlgyQ1rFld0/img.jpg)
*공통 로직 처리*

때문에 공통적으로 사용되는 로직의 경우 API Gateway에서 구현되는 것이 효율적이며, 개발 중복을 줄이는 것뿐만 아니라 표준 준수도 쉽다는 장점을 가지고 있습니다

### Load Balancing

**대용량 처리 서비스에 있어서 로드밸런싱은 필수적인 부분**이 되었는데요.

기본적으로는 여러 개의 API 서버로 부하를 분산하는 기능이 주가 되겠지만, API 서버에 장애가 발생했을 때 이를 감지해서 로드밸런싱 리스트에서 빼고, 복구되었을 때 다시 로드밸런싱 리스트에 넣는 기능들이 필요합니다. _(health-check)_

### Mediation

메디에이션 기능 중에는 **메세지 호출 변화(Message Exchange Pattern)**가 있는데요.

메세지 호출 패턴은 동기(Sync), 비동기(Async)와 같은 API를 호출하는 메세지 패턴을 정의하는 것인데, API Gateway를 이용하면 아래와 같이 동기 호출을 비동기 호출로 바꿀 수도 있습니다.

![](https://blog.kakaocdn.net/dn/ABXC5/btrRBfkYgKj/WNKVpm3kWMcxiFJ5Q0VqoK/img.jpg)
*API Gateway에서의 Message Exchance Pattern*

또한 클라이언트의 요청을 하위 서비스가 처리할 수 있도록 데이터 형식을 변형하거나, 하위 서비스의 응답 표준 포맷으로 데이터 형식을 변형하는 **메세지 포맷 변환(Message format transformation)** 기능도 있습니다.
