# Identity & Security

### Glossary

- Authentication, 인증
- Authorization, 권한부여
- Principal, 원칙
- Identity, 신원
- Claim, 주장
- Credential, 신임장
- Role, 역할


### Authentication

사용자의 신원과 관련 됨.


### Authorization

권한 부여는 허용되는 작업과 관련 됨.


### Cookie based Authentication

사용자 세션에서 사용되며, 로그인 시 생성되어 로그아웃 시 소멸 됨
 

### Dotnet Components

##### Principal

Can has multiple Identity.


##### Identity

Can has multiple Claim.
자격 증명 방법으로 운전면허, 주민등록증, 구글인증 등
User.Identities
User.Identities[i].Identity
User.Identity


##### Claim

자격 증명에 포함된 신원 정보(ID, Name, Address, ...).
User.Identity.Clams
User.Identity.Clams[i].Claim
User.Identity.Claim


### Folder Hierarchy

- Project Root
	- Pages
		- Membership or Account or Users
			Must **has a Credential**, and Credential **has the bunch of Claims**(UserName, Password, ...).
			- Login.cshtml
			- Login.cshtml.cs

| *Folder*         |       |                               | *Page*                           | Comment                                                                                         |
| ---------------- | ----- | ----------------------------- | -------------------------------- | ----------------------------------------------------------------------------------------------- |
| Project<br/>Root | Pages | Membership<br/>or<br/>Account | Login.cshtml<br/>Login.cshtml.cs | Must **has a Credential**, and Credential **has the bunch of Claims**(UserName, Password, ...). |
|                  |       |                               |                                  |                                                                                                 |

##### EF Core Generated DB Schema

![[identity-tables.png]]