# Overview
안정성, 유연성, 개방형 기술 표준 지원으로 잘 알려져 있는 오픈소스 데이터베이스로 일반적으로 "Post-GRES"로 발음.
일반적인 RDBMS와는 달리 비관계형 및 관계형 데이터 유형을 모두 지원 함. 
SQL 표준의 경우 MySQL 보다 더 잘 지원하며 쿼리가 복잡해질수록 성능이 더 잘 나오는 편임. 특히 PostGIS를 통한 Geospatial query(위치기반 질의)는 오라클 보다 더 강력함. 
약점으로 지적되었던 병렬 인덱싱도 Citus 익스텐션을 이용하여 해결 가능.
기본적인 CRUD 성능은 일반적인 RDBMS에 비해 좋지 않은 편임.
# Benefits
20여 년 후에도 PostgreSQL은 가장 잘 알려져 있고 지원되는 관계형 데이터베이스 중 하나입니다.이와 같이 PostgreSQL은 온프레미스 및 클라우드 기반 인프라 전반에 대해 확장성이 매우 뛰어난 컴퓨팅 환경을 구축하려는 개발자에게 [다양한 이점](https://www.ibm.com/kr-ko/cloud/databases-for-postgresql)을 제공합니다.
### Performance and scalability
데이터 인증 및 읽기/쓰기 속도가 필수적인 대규모 데이터베이스 시스템에서 PostgreSQL을 능가하는 것이 어렵습니다. PostgreSQL은 지형 공간 지원 및 무제한 동시성과 같이 일반적으로 독점적인 데이터베이스 기술에서만 확인할 수 있는 다양한 성능 최적화를 지원합니다. 이러한 점을 통해 PostgreSQL은 여러 데이터 유형에 대해 심층적이고, 광범위한 데이터 분석을 실행할 때 매우 효율적입니다.
### Concurrency support
여러 사용자가 동시에 데이터에 액세스하는 경우, 기존의 데이터베이스 시스템은 일반적으로 읽기/쓰기 충돌을 방지하기 위해 레코드에 대한 액세스를 차단합니다. PostgreSQL은 MVCC(Multiversion Concurrency Control)를 사용하여 동시성을 효율적으로 관리합니다. 실제로는 읽기가 쓰기를 차단하지 않고, 쓰기가 읽기를 차단하지 않습니다.
### Concurrency support
PostgreSQL은 여러 프로그래밍 언어의 호환성과 지원으로 개발자를 위한 가장 유연한 데이터베이스 중 하나입니다. Python, JavaScript, C/C++, Ruby 등 일반적인 코딩 언어는 PostgreSQL에 대한 성숙한 지원을 제공하므로 개발자는 시스템 충돌을 일으키지 않고 자신이 능숙한 언어로 데이터베이스 작업을 수행할 수 있습니다.
### Business continuity
기업은 재해 발생 시에도 지속적인 운영을 유지해야 합니다. 클라이언트와 개발자 모두가 프로덕션 데이터베이스를 언제나 사용할 수 있도록 보장하는 지속 가능한 솔루션이 필요합니다. 여러 서버에 걸친 비동기식 또는 동기식 복제 방법을 통해 서비스의 고가용성을 보장하도록 PostgreSQL을 구성할 수 있습니다.
# Feature
개발자는 엔터프라이즈 데이터베이스 배포를 통해 PostgreSQL을 사용할 때 다양한 이점을 경험합니다. PostgreSQL은 확장성이 뛰어나고 관리하기 쉬운 데이터베이스를 구축하는 동시에 여러 컴퓨팅 환경에서 원활한 복제 및 동시성을 제공하는 풍부한 기능과 확장 기능을 갖추고 있습니다.
### Point-in-time recovery
PostgreSQL을 사용하는 개발자는 데이터 복구 이니셔티브를 실행할 때 PITR(특정 시점 복구)을 통해 데이터베이스를 특정 시점으로 복원할 수 있습니다. PostgreSQL은 항상 WAL(미리 쓰기 로그)을 유지 관리하기 때문에 모든 데이터베이스 변경 사항을 기록합니다. 이렇게 하면 파일 시스템을 안정적인 시작점으로 쉽게 복원할 수 있습니다. 

대부분의 클라우드 관리형 PostgreSQL 서비스에서 이러한 작업을 자동으로 처리하므로 [pgBackRest](https://pgbackrest.org/)(ibm.com 외부 링크)와 같은 타사 도구를 사용하면 작업을 더 쉽고 안정적인 작업을 수행할 수 있습니다.
### Stored procedures
PostgreSQL은 여러 절차적 언어를 기본적으로 제공하므로 개발자는 [스토어드 프로시저](https://www.ibm.com/docs/en/i/7.4?topic=routines-stored-procedures)라고 하는 맞춤형 서브루틴을 만들 수 있습니다. 이러한 절차는 주어진 데이터베이스에서 생성 및 호출할 수 있습니다. 확장 기능을 사용하면, Perl, Python, JavaScript 및 Ruby를 비롯한 많은 다른 프로그래밍 언어의 개발을 위해 절차적 언어를 사용할 수도 있습니다.
# Common use cases

PostgreSQL은 다양한 산업 분야의 기업을 위한 이상적인 데이터베이스 솔루션입니다.이 오픈 소스 기술은 여러 일반 사용 사례에서 빛을 발합니다.
### OLTP and analytics
PostgreSQL is great for managing OLTP (Online Transaction Processing) protocols. As a general purpose OLTP database, PostgreSQL works well for a variety of use cases like e-commerce, CRMs, and financial ledgers. PostgreSQL’s SQL compliance and query optimizer also make it useful for general purpose analytics on your data.
PostgreSQL은 OLTP(온라인 트랜잭션 처리) 프로토콜을 관리하는 데 적합합니다. 범용 OLTP 데이터베이스인 PostgreSQL은 전자상거래, CRM, 재무 원장과 같은 다양한 사용 사례에 적합합니다. PostgreSQL의 SQL 규정 준수 및 쿼리 최적화 프로그램은 데이터에 대한 범용 분석에도 유용합니다.

### Geographic information systems
[PostGIS](https://postgis.net/) (link resides outside ibm.com) is an Open Geospatial Consortium (OGC) software offered as an extender to PostgreSQL. It allows PostgreSQL to support geospatial data types and functions to further enhance data analysis. By supporting geographic objects, PostgreSQL can refine sales and marketing efforts by augmenting situational awareness and intelligence behind stored data as well as help improve fraud detection and prevention.
[PostGIS](https://postgis.net/)(ibm.com 외부 링크)는 PostgreSQL의 확장 기능으로 제공되는 OGC(개방형 공간 정보 컨소시엄) 소프트웨어입니다. 이를 통해 PostgreSQL은 지형 공간 데이터 유형 및 함수를 지원하여 데이터 분석을 더욱 개선할 수 있습니다. PostgreSQL은 지리적 객체를 지원하여 저장된 데이터에 대한 상황 인식 및 인텔리전스를 강화하고, 사기 행위 탐지 및 예방을 개선하여 영업 및 마케팅 활동을 개선할 수 있습니다.
### Database consolidation
Move legacy databases to PostgreSQL while consolidating license costs, retiring servers, and cleaning up database sprawl. This can remove vendor-lock in, decrease the total cost of ownership for the databases, and improve application portability.
라이선스 비용을 통합하고, 서버를 폐기하며, 데이터베이스 확산을 정리하면서 레거시 데이터베이스를 PostgreSQL로 이동하십시오.이를 통해 벤더 종속을 제거하고, 데이터베이스의 총 소유 비용을 낮추며, 애플리케이션 이식성을 개선할 수 있습니다.