# Overview
MySQL이 오라클에 인수된 후 불확실한 라이선스 문제 해결을 위해 개발된 오픈소스 RDBMS. 2009년에 MySQL AB 출신 개발자들이 MariaDB 재단을 세워 현재까지 개발하고 있음.

무료인 MariaDB Server와 상용인 MariaDB Enterprise Server로 구분되며 일반적으로 MariaDB라고 말할때는 무료 버전인 MariaDB Server를 지칭한다.
MariaDB Server는 MariaDB 재단에서, MariaDB Enterprise Server는 MariaDB 코퍼레이션이 관리.
# Environments
### Default Data Directory

- "/etc/mysql/my.cnf"의 *datadir*
- default is "/var/lib/mysql"
# Commands
### Change root password

1. 패스워드 설정 여부 확인

```console
mysql -uroot
```

2. 위의 명령이 실패하면 로그에서 확인

```console
grep 'temporary' /bar/log/mariadb/mariadb.log
```

3. 루트로 로그인

```console
msyql -uroot -p[Your Password]
```

or

```console
mysql -uroot
```

4. 비밀번호 변경 또는 설정

```console
ALTER USER 'root'@'localhost' IDENTIFIED BY 'Your Password';
flush privileges;
exit;
```

### Show all database

```mysql
SHOW databases;
```

### Show all users

```mysql
SELECT User FROM mywql.user;
```