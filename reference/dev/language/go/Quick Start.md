## Create a Project

	$ mkdir proj_dir
	$ cd proj_dir
	$ go mod init venea.co.kr/hrs
	$ module venea.co.kr/hrs

## Build a Project

	$ go build -o output_name(.exe)
	
## Install Framework

* Gin web framework
	$ cd proj_dir
	$ go get -u github.com/git-gonic/gin

