
## Type of Test

### Unit testing

### System testing

### System Integration Testing (SIT)

전체 시스템을 이루는 개별 시스템 또는 기능들이 정상적으로 연계되어 동작하고 생성된 데이터의 무결성을 시험하여 초기 단계 결함을 감지하고 개별 시스템 또는 기능들의 초기 피드백을 받는다.
SIT의 결과는 UAT로 전달됨.

### User acceptance testing (UAT)

업무 요구 사항에 대해 개발 결과를 검증하며, 업무 요구 사항에 익숙한 최종 사용자가 수행.
출시 또는 오픈 전 수행되는 마지막 시험.