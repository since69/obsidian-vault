
| Phase                | Activity            | Task               | Artifact                  |
| -------------------- | ------------------- | ------------------ | ------------------------- |
| Project Planing (PP) | PP10. TFT 구성 및 테일러링 | PP11. 사업TFT 구성     | PP11-1. TFT 구성계획서         |
|                      |                     | PP12. 방법론 테일러링     | PP12-1. 방법론 테일러링 결과서      |
|                      | PP20. 개발 사전준비       | PP21. XXX 시스템 개발준비 | -                         |
| Analysis (AN)        | AN10. 요구사항분석        | AN11. 요구사항 수집      | AN11-1. 면담계획서             |
|                      |                     |                    | AN11-2 면담결과서              |
|                      |                     | AN12. 요구사항 정의      | AN12-1. 요구사항 정의서          |
|                      |                     | AN13. UseCase 기술   | AN13-1. UseCase 명세서       |
|                      |                     | AN14.  요구사항 추적     | AN14-1. 요구사항 추적표          |
|                      | AN20. 업무/데이터 분석     | AN21. 업무분석         | AN21-1. AS-IS비즈니스프로세스 정의서 |
|                      |                     |                    | AN21-2. AS-IS비즈니스업무흐름도    |
|                      |                     |                    | AN21-3. TO-BE비즈니스프로세스 정의서 |
|                      |                     |                    | AN21-2. TO-BE비즈니스업무흐름도    |
|                      |                     | AN22. E데이터분석       | AN22-1. AS-IS 데이터분석서      |
|                      |                     |                    | AN22-2. AS-IS 표준사전정의서     |
|                      |                     |                    | AN22-3. 전환대상업무 및 범위정의서    |
|                      | AN30. 아키텍처분석        |                    |                           |
|                      |                     |                    |                           |

Original Document is [특허청 소프트웨어(S/W) 개발방법론](https://www.cisp.or.kr/wp-content/uploads/2014/12/%EC%86%8C%ED%94%84%ED%8A%B8%EC%9B%A8%EC%96%B4SW-%EA%B0%9C%EB%B0%9C%EB%B0%A9%EB%B2%95%EB%A1%A0_%ED%8A%B9%ED%97%88%EC%B2%AD-2014-12.pdf)