
# CSS Naming Conventions that Will Save You Hours of Debugging

CSS 파일이 커지고 복잡해지면 발생할 수 있는 유지보수의 어려움을 해결하기 위한 명명법.


### 하이픈(-)으로 구분된 문자열 사용

자바스크립트등에서 사용되는 카멜 케이스와 C#등에서 사용되는 파스칼 케이스는 CSS에 적합하지 않으며 스네이크 케이스를 권장함.

- 카멜 케이스를 사용한  자바스크립트
```JAVASCRIPT
var redBox = document.getElementById('...')
```

- 카멜 케이스를 사용한 CSS (X)
```css
.redBox {
	border: 1px solid red;
}
```

- 스네이크 케이스를 사용한 CSS (O)
```css
.red-box {
	border: 1px solid red;
}
```

스네이크 케이스는 CSS 명명법의 표준이며 가독성이 뛰어나며, CSS 속성 명명 규칙과도 일치.

- 잘못된 예
```css
.some-class {
	fontWeight: 10em;
}
```

- 잘된 예
```css
.some-class {
	font-weight: 10em;
}
```
  
 
 ### BEM 명명법

일반적으로 CSS 명명법은 다음과 같은 3가지 문제를 해결할 수 있음
- CSS Selector 이름을 보고 **무엇을 하는지** 알 수 있다.
- CSS Selector 이름을 보고 **어디서 사용하는지** 알 수 있다.
- CSS Selector 이름을 보고 **Class들의 관계를** 알 수 있다.

다음은 BEM 명명법을 활용한 클래스 명
```css
.nav-secondary {
	...
}
.nav__header {
	...
}
```


### 요약

BEM은 전체 사용자 인터헤이스를 재사용 가능한 작은 구성 요소들로 분리함. 이를 위해 Block, Element, Modifier로 구분된 명명법을 사용.

#### B is Block

![[stick-man.png]]
**BEM**의  **B**는 **Block**을 의미함. 

스틱맨(인체 기호)은 Block과 같은 Component에 해당하며, 실제 개발에서 Block은 사이트의 *Nav*, *Header*, *Footer* 등에 해당.

BEM 몀명법에 따른 스틱맨 Component의 이상적인 클래스 명은 *stick-man*이 될 수 있으며 아래와 같이 표시할 수 있음,

```css
.stick-man {
	...
}
```

이처럼 구분된 문자열을 사용.
![[stick-man2.png]]


#### E is Element

**BEM**의 **E**는 **Elements**의 의미.

전반적으로 Bolock이 따로 고립된 채로 있는 경우는 드물음. 예를 들어, 스틱맨은 머, 두개의 팔 그리고 다리를 가지고 있음.

![[stick-man3.png]]
머리, 팔, 그리고 다리는 모두 Component 안에 있는 구성요소(Elements) 임.
BEM 명명 규칙을 사용하여 **두 개의 밑줄**을 추가하고 구성요소(Element)의 이름을 추가.

**예시**
```css
.stick-man__head {
	...
}

.stick-man__arms {
	...
}

.stick-man__feet {
	...
}
```


#### M is Modifier

**BEM**의 **M**는 **Modifires**의 의미.

스틱맨의 색을 빨간색 또는 파란색으로 꾸며주고 싶다면 어떻게 해야 할까?

![[stick-man4.png]]
실 예로 버튼을 생각할 수 있음. 버튼을 파란색이나 빨간색으로 꾸며줘야 할 때 이런 수식어(Modifier)를 BEM으로 표현 가능.

**두개의 하이픈(-)**과 수식어(modofier) 클래스 명을 추가합니다.

**예시**
```css
.stick-man--blue {
	...
}

.stick-man--red {
	...
}
```

위의 예시에서는 수식어(Modifier)가 추가된 상위 Component를 보여줌,

머리 크기가 다른 스틱맨인 있다면 어떻게 할까?
![[stick-man5.png]]
이 경우 스틱맨 Component 내부 Elememnt(머리)가 수정 됨. 머리는 하위 Component라는걸 명심해야 함.

.stick-man은 Block에 해당하고, .stick-man__head는 구성요소(Elememt)에 해당함.

아래 예시에서 볼 수 있듯이 하이픈을 아리와 같이 사용할 수 있음.

**예시**
```css
.stick-man__head--small {
	...
}

.stick-man__head--big {
	...
}
```

위의 예시에서 **두 개의 하이픈**은 수식어(Modifire)를 나타내는 데 사용 됨.

지금까지 BEM이 작동하는 기본 방식을 설명하였음.


### 왜 명명 규칙(Naming Conventions)을 사용하는가?

```
Computer Science에는 두 가지 어려운 문제가 있다.
캐시 뮤효화(Cache invlidation)와 이름 붙이가.
- Phil Karlton
```

명명은 귀찮은 일이지만 효과적인 명명은 개발 및 추후 유지보수 시간을 절약할 수 있음.

정확한 CSS 명명 규칙을 따른다면, 코드를 읽기 쉽고 유지 보수하기 쉽게 만들어 줌.

만약 BEM 명명 규칙을 선택한 경우, Component와 Blokc 간의 관계를 쉽게 확인할 수 있음.


### CSS Names with Javascript Hooks

만약 다음과 같은 HTML 코드를 건네받았다고 생각해보자.
```html
<div class="siteNavigation">
</div>
```

그리고 다음과 같이 코드를 수정했다고 생각해보자.
```html
<div class="site-navigation">
</div>
```

수정된 내용은 카멜 명명법을 CSS에서 요구되는 스네이크 명명법으로 변경되었다. 
하지만 이 코드는 자바스크립트에서 문제를 발생시키낟.
```javascript
// the Javascript code
const nav = document.querySelector('.siteNavigation');
```

스타일 시트에서 클래스 명이 변경되어 위의 자바스크립트 코드에서 nav 변수는 null이 됨.

이런 경우를 방지하기 위해서는 아래와 같은 여러가지 방법을 사용할 수 있다.

#### 1. js-class 이름 사용

버그를 줄이는 한 가지 방법으로 DOM 요소와의 관계를 나타내기 위해 **js-\*** 클래스 명명법을 사용.

**에시 (HTML)**
```html
<div class="site-navigation js-site-navigation">
...
</div>
```

**에시 (Javasctip)**
```javascript
// the Javascript code
const var = document.querySelector('.js-site-navigation');
```

이런 규칙을 통해 **js-site-navigation** 클래스 명을 본 사람은 해당 DOM 요소가 Javascript와 관계 있다고 이해할 수 있음.

#### 2. Rel 속성 사용

별로 좋은 방법은 아니라고 생갹되지만 이렇게 사용하는 사람들이 종종 있음.
```html
<link rel="stylesheet" type="text/css" href="main.css">
```

기본적으로 rel 특성은 링크된 자원이 참조하는 문서와의 관계를 정의함.
이 방법을 사용하는 사람은 다음과 깉이 HTML을 작성할 것임.
```html
<div class="site-navigation" rel="js-site-navigation">
</div>
```
그리고 자바스크립트는 다음과 같이 작성됨.
```javascript
const nav = documen.querySelector("[rel='js-site-navigation']");
```

이 방법이 주장하는 바는 "자바스크립트와 관계가 있어, 그래서 나는 rel 속성으로 그것을 표현할거야"임.

좋은 방법은 아니라고 생각되지만 웹은 같은 문제를 해결하기 위한 각기 다른 "방법"들을 가진 거대한 장임.

#### 3. Data 속성 사용하지 않기

일부 개발자는 data 속성을 자바스크립트를 위해 사용하지만 이것은 정말 옳지 않은 방법임. 정의에 따르면 data 속성은 **사용자 데이터 저장**에 사용 됨.
![[js-custom-data.png]]

트위터에 보이는 것처럼 data 속성을 적절히 사용.

수정 #1: 답변에 잇는 일부 놀라운 사람들의 언급에 의하면 rel 속성을 사용하는 경우, 특정 상황에 data 속성을 사용해도 된다고 함. 결정은 개발자의 몫?


### Bonus Tip: 추가 CSS 주석 작성

이것은 명명 규치과는 관계가 없지만, 개발 시간을 단축시켜줄것 임.

많은 웹 개발자들이 자바스크립트 주석을 작성하지 않으려고 하거나 몇 개만 남기지만, 가능한 많은 CSS 주석을 남겨야 할 필요가 있음.

CSS는 우아한 언어가 아니기 때문에, 잘 구축된 주석은 코들을 이해하고 유지보수를 쉽게 할 수 있도록 함.

이것은 번거러운 일이 아님.

[Bootstrap 소스코드](https://github.com/twbs/bootstrap/blob/v4-dev/scss/_carousel.scss)가 얼마나 잘 설명을 하고 있는지 확인할 수 있음.

**color: red**가 빨간색을 나타낸다는 점을 주석으로 달 필요가 없지만, 덜 명백한(추가적인 설명이 필요한) CSS를 다루고 있다면 반드시 주석을 달아야 함.

### Reference

[Get the free book](https://ohansemmanuel.us17.list-manage.com/subscribe?u=6fa63587721dce8a5eb987a2a&id=ba27bc2a77)
![[reference-book.png]]
- [Original Text: CSS Naming Conventions that Will Save You Hours of Debugging - Ohans Emmanuel](https://medium.freecodecamp.org/css-naming-conventions-that-will-save-you-hours-of-debugging-35cea737d849) 
- [CSS 디버깅 시간을 절약할 수 있는 CSS 명명 규칙 - Early adopter](https://www.vobour.com/-css-%EB%94%94%EB%B2%84%EA%B9%85-%EC%8B%9C%EA%B0%84%EC%9D%84-%EC%A0%88%EC%95%BD-%ED%95%A0-%EC%88%98%EC%9E%88%EB%8A%94-css-%EB%AA%85%EB%AA%85-%EA%B7%9C%EC%B9%99)

