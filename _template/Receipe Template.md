
---
title: Rceipe for food-name
author: Joohyoung Kim
created at: <% tp.file.creation_date() %>
modified at: <% tp.file.last_modified_date() %>

---

# <% tp.file.title %>

### 재료

- 첫 번째 재료
- 두 번째 재료

### 조리법 

1. 첫 번째 할 일
2. 두 번째 할 일

** Original **: [Reference Title](https://www.site.com/)
** Author **: [Name](https://www.site.com/)

