
This project is a awesome project.

## Build


### Tools

- Microsoft Visual Studio 2022

### Libraries

- Microsoft Dotnet 6

## Installation

Use the install program.

## Usage

Run the main application in windows explorer or start menu.

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License

[MIT](https://choosealicense.com/licenses/mit/)